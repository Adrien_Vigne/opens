/*
 * myPWM.h
 *
 *  Created on: 4 mars 2019
 *      Author: Gurvan
 *
 *      Gus 03-03-2019
 *      from example epwm_ex2_updown_aq.c
 *
 *      Gus 02-07-2019
 *          fix frequency 35kHz to 20kHz
 */

#ifndef MYPWM_H_
#define MYPWM_H_

// Includes
#include "driverlib.h"

// settings
//names
#define PWMa    1U // PWM 1A
#define PWMb    2U // PWM 2A
#define PWMc    3U // PWM 3A
#define PWMd    4U // PWM 1B
#define PWMe    5U // PWM 2B
#define PWMf    6U // PWM 3B
#define PWMg    7U // PWM 4A
#define PWMh    8U // PWM 4B

//PWM settings
#define EPWM1_TIMER_TBPRD  1250U // =f0/fPWM (f0=25,000,000Hz), fPWM = 20kHz
#define EPWM2_TIMER_TBPRD  1250U
#define EPWM3_TIMER_TBPRD  1250U
#define EPWM4_TIMER_TBPRD  1250U
#define EPWM_INIT_CMP      0U



// Function Prototypes
//
void initPWMs(void);// Initialize the 3 modules PWM
void updatePWM(uint16_t channel, float alpha); // change PWM ratio

void initPWM1(void);// Initialize the PWM module #1
void initPWM2(void);// Initialize the PWM module #2
void initPWM3(void);// Initialize the PWM module #3
void initPWM4(void);// Initialize the PWM module #4

void startPWM(void); // Enable sync and clock to PWM
void stopPWM(void); // Stop sync and clock to PWM






















#endif /* MYPWM_H_ */
