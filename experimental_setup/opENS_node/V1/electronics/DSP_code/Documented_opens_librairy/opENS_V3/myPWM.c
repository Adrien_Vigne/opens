/*
 * myPWM.c
 *
 *  Created on: 4 mars 2019
 *      Author: Gurvan
 *
 *      Gus 03-03-2019
 *
 *      from example epwm_ex2_updown_aq.c
 *
 *      Julien 04-24-19
 *      Rajout config EPWM1 pour les interruptions sur PWM1
 *      rajout alpha en float entre 0 et 1
 *
 *      Julien et Gurvan 21-06-19
 *      synchronisation des PWM
 *
 *      Gus 02-07-2019
 *          fix frequency 35kHz to 20kHz
 */

#include "myPWM.h"

// initEPWM123 #############################################
//
void initPWMs() // initialize used ePWM modules
{

    stopPWM();

    // Set PWM
    initPWM3();
    initPWM1();
    initPWM2();
    initPWM4();

    // Synchronize the PWM
    EPWM_forceSyncPulse(EPWM3_BASE);
    EPWM_forceSyncPulse(EPWM2_BASE);
    EPWM_forceSyncPulse(EPWM1_BASE);
    EPWM_forceSyncPulse(EPWM4_BASE);

    // Disable future unwanted sync
    EPWM_disablePhaseShiftLoad(EPWM1_BASE);
    EPWM_disablePhaseShiftLoad(EPWM2_BASE);
    EPWM_disablePhaseShiftLoad(EPWM3_BASE);
    EPWM_disablePhaseShiftLoad(EPWM4_BASE);


}

// stopPWM #############################################
//
void stopPWM() // Stop sync and clock to PWM
{
    //
    // Disable sync(Freeze clock to PWM as well)
    //
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
}

// startPWM #############################################
//
void startPWM() // Enable sync and clock to PWM
{
    //
    // Enable sync and clock to PWM
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
}

// initPWM1 #############################################
//
void initPWM1() // initialize ePWM module 1
    {

    //
    // Configure pins : GPIO0/1
    //
    GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_0_EPWM1A);
    GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_1_EPWM1B);

    //
    // Set-up TBCLK
    //
    EPWM_setTimeBasePeriod(EPWM1_BASE, EPWM1_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM1_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM1_BASE, 0U);

    //
    // Set Compare values
    //
    EPWM_setCounterCompareValue(EPWM1_BASE,
                                EPWM_COUNTER_COMPARE_A,
                                EPWM_INIT_CMP);
    EPWM_setCounterCompareValue(EPWM1_BASE,
                                EPWM_COUNTER_COMPARE_B,
                                EPWM_INIT_CMP);

    //
    // Set up counter mode
    //
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_enablePhaseShiftLoad(EPWM1_BASE);  // enable or disable
    EPWM_setClockPrescaler(EPWM1_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

/*    // Sync
    EPWM_setCountModeAfterSync(EPWM1_BASE,EPWM_COUNT_MODE_UP_AFTER_SYNC);
    EPWM_setSyncOutPulseMode(EPWM1_BASE,EPWM_SYNC_OUT_PULSE_ON_SOFTWARE);*/


    //
    // Set up shadowing
    //
    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE,
                                         EPWM_COUNTER_COMPARE_A,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE,
                                         EPWM_COUNTER_COMPARE_B,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);

    //
    // Set actions
    //
    EPWM_setActionQualifierAction(EPWM1_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
    EPWM_setActionQualifierAction(EPWM1_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);

    //
    // Interrupt where we will change the Compare Values
    // Select INT on Time base counter zero event,
    // Enable INT, generate INT on 1th event
    //
    EPWM_setInterruptSource(EPWM1_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM1_BASE);
    EPWM_setInterruptEventCount(EPWM1_BASE, 1U);

}

// initPWM2 #############################################
//
void initPWM2() // initialize ePWM module 2
    {
    //
    // Configure pins : GPIO2/3
    //
    GPIO_setPadConfig(2, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_2_EPWM2A);
    GPIO_setPadConfig(3, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_3_EPWM2B);

    //
    // Set-up TBCLK
    //
    EPWM_setTimeBasePeriod(EPWM2_BASE, EPWM2_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM2_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM2_BASE, 0U);

    // Sync to EPWM1
    EPWM_setupEPWMLinks(EPWM2_BASE, EPWM_LINK_WITH_EPWM_1, EPWM_LINK_TBPRD);// now EPWM2 counter is sync to EPWM1

/*    // Sync
    EPWM_setCountModeAfterSync(EPWM2_BASE,EPWM_COUNT_MODE_UP_AFTER_SYNC);
    EPWM_setSyncOutPulseMode(EPWM2_BASE,EPWM_SYNC_OUT_PULSE_ON_SOFTWARE);*/

    //
    // Set Compare values
    //
    EPWM_setCounterCompareValue(EPWM2_BASE,
                                EPWM_COUNTER_COMPARE_A,
                                EPWM_INIT_CMP);
    EPWM_setCounterCompareValue(EPWM2_BASE,
                                EPWM_COUNTER_COMPARE_B,
                                EPWM_INIT_CMP);

    //
    // Set up counter mode
    //
    EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_enablePhaseShiftLoad(EPWM2_BASE);
    EPWM_setClockPrescaler(EPWM2_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);
    //
    // Set up shadowing
    //
    EPWM_setCounterCompareShadowLoadMode(EPWM2_BASE,
                                         EPWM_COUNTER_COMPARE_A,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM2_BASE,
                                         EPWM_COUNTER_COMPARE_B,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);

    //
    // Set actions
    //
    EPWM_setActionQualifierAction(EPWM2_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM2_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(EPWM2_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
    EPWM_setActionQualifierAction(EPWM2_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);

/*    //
    // Interrupt where we will change the Compare Values
    // Select INT on Time base counter zero event,
    // Enable INT, generate INT on 1th event
    //
    EPWM_setInterruptSource(EPWM2_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM2_BASE);
    EPWM_setInterruptEventCount(EPWM2_BASE, 1U);*/

}

// initPWM3 #############################################
//
void initPWM3() // initialize ePWM module 3
    {
    //
    // Configure pins : GPIO4/5
    //
    GPIO_setPadConfig(4, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_4_EPWM3A);
    GPIO_setPadConfig(5, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_5_EPWM3B);

    //
    // Set-up TBCLK
    //
    EPWM_setTimeBasePeriod(EPWM3_BASE, EPWM3_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM3_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM3_BASE, 0U);

    // Sync to EPWM1
    EPWM_setupEPWMLinks(EPWM3_BASE, EPWM_LINK_WITH_EPWM_1, EPWM_LINK_TBPRD);// now EPWM2 counter is sync to EPWM1

 /*   // Sync
    EPWM_setCountModeAfterSync(EPWM3_BASE,EPWM_COUNT_MODE_UP_AFTER_SYNC);
    EPWM_setSyncOutPulseMode(EPWM3_BASE,EPWM_SYNC_OUT_PULSE_ON_SOFTWARE);*/

    //
    // Set Compare values
    //
    EPWM_setCounterCompareValue(EPWM3_BASE,
                                EPWM_COUNTER_COMPARE_A,
                                EPWM_INIT_CMP);
    EPWM_setCounterCompareValue(EPWM3_BASE,
                                EPWM_COUNTER_COMPARE_B,
                                EPWM_INIT_CMP);

    //
    // Set up counter mode
    //
    EPWM_setTimeBaseCounterMode(EPWM3_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_enablePhaseShiftLoad(EPWM3_BASE);
    EPWM_setClockPrescaler(EPWM3_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // Set up shadowing
    //
    EPWM_setCounterCompareShadowLoadMode(EPWM3_BASE,
                                         EPWM_COUNTER_COMPARE_A,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM3_BASE,
                                         EPWM_COUNTER_COMPARE_B,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);

    //
    // Set actions
    //
    EPWM_setActionQualifierAction(EPWM3_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM3_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(EPWM3_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
    EPWM_setActionQualifierAction(EPWM3_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);

/*    //
    // Interrupt where we will change the Compare Values
    // Select INT on Time base counter zero event,
    // Enable INT, generate INT on 1th event
    //
    EPWM_setInterruptSource(EPWM3_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM3_BASE);
    EPWM_setInterruptEventCount(EPWM3_BASE, 1U);*/

}

// initPWM4 #############################################
//
void initPWM4() // initialize ePWM module 3
    {
    //
    // Configure pins : GPIO4/5
    //
    GPIO_setPadConfig(6, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_6_EPWM4A);
    GPIO_setPadConfig(7, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_7_EPWM4B);

    //
    // Set-up TBCLK
    //
    EPWM_setTimeBasePeriod(EPWM4_BASE, EPWM4_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM4_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM4_BASE, 0U);

    // Sync to EPWM1
    EPWM_setupEPWMLinks(EPWM4_BASE, EPWM_LINK_WITH_EPWM_1, EPWM_LINK_TBPRD);// now EPWM4 counter is sync to EPWM1

 /*   // Sync
    EPWM_setCountModeAfterSync(EPWM4_BASE,EPWM_COUNT_MODE_UP_AFTER_SYNC);
    EPWM_setSyncOutPulseMode(EPWM4_BASE,EPWM_SYNC_OUT_PULSE_ON_SOFTWARE);*/

    //
    // Set Compare values
    //
    EPWM_setCounterCompareValue(EPWM4_BASE,
                                EPWM_COUNTER_COMPARE_A,
                                EPWM_INIT_CMP);
    EPWM_setCounterCompareValue(EPWM4_BASE,
                                EPWM_COUNTER_COMPARE_B,
                                EPWM_INIT_CMP);

    //
    // Set up counter mode
    //
    EPWM_setTimeBaseCounterMode(EPWM4_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_enablePhaseShiftLoad(EPWM4_BASE);
    EPWM_setClockPrescaler(EPWM4_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // Set up shadowing
    //
    EPWM_setCounterCompareShadowLoadMode(EPWM4_BASE,
                                         EPWM_COUNTER_COMPARE_A,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM4_BASE,
                                         EPWM_COUNTER_COMPARE_B,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);

    //
    // Set actions
    //
    EPWM_setActionQualifierAction(EPWM4_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM4_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(EPWM4_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
    EPWM_setActionQualifierAction(EPWM4_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);

}

// updatePWM #############################################
//  use PWMa, PWMb, PWMc, ... PWMf to refer to a PWM
//  alpha must be a float between 0 and 1
void updatePWM(uint16_t channel, float alpha) // change PWM ratio
{
    /*//names
    #define PWMa    1U // PWM 1A
    #define PWMb    2U // PWM 2A
    #define PWMc    3U // PWM 3A
    #define PWMd    4U // PWM 1B
    #define PWMe    5U // PWM 2B
    #define PWMf    6U // PWM 3B
    #define PWMg    7U // PWM 4A
    #define PWMh    8U // PWM 4B*/

    // Counter is included in [0;1250]
    alpha=alpha*1250;

    switch (channel){
    case 1 : // PWM 1A
       EPWM_setCounterCompareValue(EPWM1_BASE,
                                        EPWM_COUNTER_COMPARE_A,
                                        alpha);
       break;
    case 2 : // PWM 2A
       EPWM_setCounterCompareValue(EPWM2_BASE,
                                        EPWM_COUNTER_COMPARE_A,
                                        alpha);
       break;
    case 3 : // PWM 3A
       EPWM_setCounterCompareValue(EPWM3_BASE,
                                        EPWM_COUNTER_COMPARE_A,
                                        alpha);
       break;
    case 4 : // PWM 1B
       EPWM_setCounterCompareValue(EPWM1_BASE,
                                        EPWM_COUNTER_COMPARE_B,
                                        alpha);
       break;
    case 5 : // PWM 2B
       EPWM_setCounterCompareValue(EPWM2_BASE,
                                        EPWM_COUNTER_COMPARE_B,
                                        alpha);
       break;
    case 6 : // PWM 3B
       EPWM_setCounterCompareValue(EPWM3_BASE,
                                        EPWM_COUNTER_COMPARE_B,
                                        alpha);
       break;
    case 7 : // PWM 4A
       EPWM_setCounterCompareValue(EPWM4_BASE,
                                        EPWM_COUNTER_COMPARE_A,
                                        alpha);
           break;
    case 8 : // PWM 4B
       EPWM_setCounterCompareValue(EPWM4_BASE,
                                        EPWM_COUNTER_COMPARE_B,
                                        alpha);
           break;
    default :
        asm("   ESTOP0");// error in channel selection
    }

}
