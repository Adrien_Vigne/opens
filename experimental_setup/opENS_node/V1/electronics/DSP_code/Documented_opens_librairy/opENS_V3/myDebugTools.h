/*
 * myDebugTools.h
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *
 *      Here are the tools I use to debug and send data though serial connection.
 *      The serial interface through keyboard is also managed here.
 */

#ifndef MYDEBUGTOOLS_H_
#define MYDEBUGTOOLS_H_

#include "myheader.h"

//
// Function Prototypes
//
void info2serial(void);
void LowPriorityLoop_logs(void);
void LowPriorityLoop_KeyboardInterface(uint16_t receivedChar);
void HighSpeedLogs(void);

// Global variables DEBUG and logs
#define LOGpoints 1000
extern float mem1[LOGpoints];
extern float mem2[LOGpoints];
extern float mem3[LOGpoints];
extern uint16_t LOGcount;

//### Serial connection ###
extern bool SendSerial;

#endif /* MYDEBUGTOOLS_H_ */
