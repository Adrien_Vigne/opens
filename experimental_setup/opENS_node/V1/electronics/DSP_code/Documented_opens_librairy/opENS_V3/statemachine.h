/*
 * statemachine.h
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      Contains the routines for state machine control.
 *
 */

#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_

#include "myheader.h"

//
// Definition of the states
//
#define START_UP 0
#define CALIB_SENSOR 1
#define CALIB_SENSOR2 2
#define STARTPFC 3
#define STARTPFC2 4
#define PARKCONTROL 5
#define IDLE_STATE 6 // Idle state

// Definition of durations for states
// counter time by high speed calls (x50�s or /20kHz)
#define TIME_START_UP 100000 // 5s
#define TIME_CALIB_SENSOR 120000 // 6s
#define TIME_STARTPFC 100000 // 5s


// Globals
extern uint16_t MainState;
extern int32_t compt;

#endif /* STATEMACHINE_H_ */
