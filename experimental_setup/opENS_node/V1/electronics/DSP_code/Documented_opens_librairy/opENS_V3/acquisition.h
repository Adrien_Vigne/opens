/*
 * acquisition.h
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      routines for analog acquisitions:
 *          - zeroing and sensors' gains
 *          - statistics:
 *              - averaging/filtering
 *              - RMS� and RMS*/

#ifndef ACQUISITION_H_
#define ACQUISITION_H_

#include "myheader.h"

//
// Function Prototypes
//
void sensorAcquisition(void);
void updateStatistics(void);
void applyCalibration(void);

//
// Globals
//
//### Zero of sensors ###
extern uint16_t adcA0zero;
extern uint16_t adcA2zero;
extern uint16_t adcB2zero;
extern uint16_t adcC2zero;
extern uint16_t adcA3zero;
extern uint16_t adcB3zero;
extern uint16_t adcC3zero;
extern uint16_t adcA1zero;
extern uint16_t adcA4zero;
extern uint16_t adcB4zero;


//### Gains of the sensors
extern float gainIA;
extern float gainIB;
extern float gainIC;
extern float gainIRES;
extern float gainVA;
extern float gainVB;
extern float gainVC;
extern float gainVRES;
extern float gainVUDC;
extern float gainIA2;


//### SG currents ###
extern float ia, ia2;
extern float ib;
extern float ic;
extern float ires;
extern float vres;
extern float vaG, vAN; // G: GND, N:NEUTRAL
extern float vbG, vBN; // G: GND, N:NEUTRAL
extern float vcG, vCN; // G: GND, N:NEUTRAL
extern float vNG,vNGmoy, vNGeff; // G: GND, N:NEUTRAL
extern float vudc;

//### Statistics ###
//Coefficients for the average filters : Xmoy (t+1) = COEFF_AV1*X (t) + COEFF_AV2*Xmoy (t)
// COEF_AV gives a convergence on a 50Hz sine + offset within 4s for 1% error, 6s for 0.4%
#define COEFF_AV1 0.00005
#define COEFF_AV2 (1-COEFF_AV1)
// COEF_AVDC gives a convergence on a 5V 100Hz sine + 300V offset within 0.1s and reject 80% of ripples.
#define COEFF_AVDC1 0.003
#define COEFF_AVDC2 (1-COEFF_AVDC1)

// moy stands for average, eff stands for RMS.
extern float Iamoy, Iaeff;
extern float Ibmoy, Ibeff;
extern float Icmoy, Iceff;
extern float Iresmoy, Ireseff;
extern float Vamoy, Vaeff;
extern float Vbmoy, Vbeff;
extern float Vcmoy, Vceff;
extern float Vresmoy, Vreseff;
extern float Vudcmoy, Vresmax, Vresmaxmoy;
extern float Ia2moy, Ia2eff;

#endif /* ACQUISITION_H_ */
