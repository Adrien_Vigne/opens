/*
 * myUSBserial.h
 *
 *  Created on: 6 mars 2019
 *      Author: Gurvan
 *
 *      Gus 06-03-2019
 *
 *      from sci_ex3_echoback.c
 *
 *      16-10-2019 change pow to fpown to be compatible with "math.h"
 */

#ifndef MYUSBSERIAL_H_
#define MYUSBSERIAL_H_

// Includes
#include "driverlib.h"
#include "device.h"


// settings
#define my_LSPCLK_FREQ    (DEVICE_SYSCLK_FREQ / 8) // I don't know why, the frequency is wrong (halved)
#define myBaudRate              115200 //bauds

// Function Prototypes
//
void initUSBserial(void); // initialize the serial connection through USB
void USBserialSend(unsigned char *msg); // send a message through serial USB
void itoa(int32_t value, unsigned char* result, uint16_t base);// convert Iteger To Output String
void ftoa(float n, unsigned char* res, uint16_t afterpoint);
float fpown(float x, uint16_t n);










#endif /* MYUSBSERIAL_H_ */
