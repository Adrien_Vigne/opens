// Configuration for opENS A1
// first prototype VprotoElec1
#ifndef CONFIG_ID

// ID name of the hardware
#define CONFIG_ID "A1"
#define CALIBGAINIA 0.00661 //2019-10-22
#define CALIBGAINIB 0.00543 //2019-10-22
#define CALIBGAINIC 0.00556 //2019-10-22
#define CALIBGAINIRES 0.0061
#define CALIBGAINVA 0.099
#define CALIBGAINVB 0.099
#define CALIBGAINVC 0.099
#define CALIBGAINVRES 0.178
#define CALIBGAINVUDC 0.0911 // ~1/11

#define CALIBGAINIA2 0.0488281 // Amp/count LEM LTS 25-NP + Rdiv 3.3/5 - 2020-01-17

//Output capacitor (F) x pulsation (2pi 50Hz)
#define CAPAW_A 314.1*10.00e-6 //default
#define CAPAW_B 314.1*10.00e-6 //Default
#define CAPAW_C 314.1*10.00e-6 //Default

#endif
