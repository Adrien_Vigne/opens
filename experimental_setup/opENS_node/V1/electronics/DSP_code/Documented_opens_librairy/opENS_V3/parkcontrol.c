/*
 * parkcontrol.c
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      All the routines for d-q park control
 */

#include "parkcontrol.h"

// Variable initialization
//
//### Park transform variables ###
uint16_t phaseSG=0;// phase counter, use sinTable[phase>>8], i.e. phase from 0 to 256*2^8=65535
float previousVdSG=0;//memory of previous D voltage in DQ plan, for 3 phase PLL control
float FreqSG=refFreqSG;
float sinSyncSG=0;

float iRefq=0;
float iRefd=0;
float d=0;
float q=0;
float O=0;
float id=0;
float iq=0;
float i0=0;

float derror=0;
float qerror=0;
//float Oerror=0; unused
float dpreviouserror=0;
float qpreviouserror=0;
float Opreviouserror=0;
float kPdq = 0.0054;// TODO set params
float kIdq = -0.0050;

/*//### EDF rectifier control ###
float udcerror=0, udcpreviouserror=0;// Udc PI controller
#define kPudc 1
#define kIudc -.99995
float iRefAmpedf=0, iRefEdf=0; // amplitude of the reference EDF current, reference EDF current
float previousInputSignEDF=0;
float sinEDF=0;// sinus envelop to generate current reference (= output of PLL)
float cosEDF=0;// cosEDF is the projection signal, i.e. 90� phase in advance compared to sinEDF
float projSinEDF=0;// projected signal
float loopFilterOut=0, previous1loopFilterOut=0;
float NCOedf_cpt=0; // phase counter for Numerical Controlled Oscillator
#define NCO_0  163.84 // valeur pour osciller � fc_vco
//Current controller
float edfPwm=0, previousEdfErr=0;// Ires PI controller
#define kPedf 0.024127 // 2019-10-17
#define kIedf -0.024117
int16_t edfPwmState=0; // state of #2 V, to compensate vres measure.
float IampLimit = 0; // current amplitude limitation*/

//
// #######################################################################
// Function implementations


// #######################################################################
// # PARK-1
// # 2020-02-03 implementation of homo-polar
void parkinverse(float d, float q, float O, uint16_t ph){

    float tmp1;
    float tmp2;

    // Compute inverse park transform
    //cos
    tmp1 = sinTable[(ph+16384)>>8];//sin(x+90�)
    //sin
    tmp2 = sinTable[ph>>8];
    // A = cos * Vd - sin * Vq
    float A = ROOT2OVER3*(tmp1 * d - tmp2 * q);

    //cos(x-2pi/3)
    tmp1 = sinTable[(ph-5461)>>8];//sin(x-120�+90�)
    //sin(x-2pi/3)
    tmp2 = sinTable[(ph-21845)>>8];//sin(x-120�)
    float B = ROOT2OVER3*(tmp1 * d - tmp2 * q);

    //cos(x+2pi/3)
    tmp1 = sinTable[(ph+(uint16_t)38229)>>8];//sin(x+120�+90�)
    //sin(x+2pi/3)
    tmp2 = sinTable[((uint16_t)ph+21845)>>8];//sin(x+120�)
    float C = ROOT2OVER3*(tmp1 * d - tmp2 * q);

    // Apply homo-polar correction
    A += O;
    B += O;
    C += O;

    // check ranges
    A = (A<-.5) ? -.5 : A; A = (A>.5) ? .5 : A;
    B = (B<-.5) ? -.5 : B; B = (B>.5) ? .5 : B;
    C = (C<-.5) ? -.5 : C; C = (C>.5) ? .5 : C;

    // change PWM ratio
    updatePWM(PWMa, .5+A);// sortie puissance A = #1U
    updatePWM(PWMb, .5+B);// sortie puissance B = #1V
    updatePWM(PWMc, .5+C);// sortie puissance C = #1W

}




// #######################################################################
// # This routine is the DQ control of the currents of the smart grid.
// # 2020-02-03 implementation of homo-polar control.
void DQcontrolSG()
{

       //### Compute park transform ###

       //Gurvan: 03-02-2020 add i0
       // i0=1/3 * (ia + ib + ic)
       i0 = 0.333333*(ia+ib+ic);
       // id=ROOT2OVER3*(ia*cos(phaseSG)+ib*cos(phaseSG-2pi/3)+ic*cos(phaseSG+2pi/3));
       id= ROOT2OVER3*(ia*sinTable[(phaseSG+16384)>>8]+ib*sinTable[(phaseSG-5461)>>8]+ic*sinTable[(phaseSG+(uint16_t)38229)>>8]);
       // iq=ROOT2OVER3*(ia*-sin(phaseSG)+ib*-sin(phaseSG-2pi/3)+ic*-sin(phaseSG+2pi/3));
       iq= ROOT2OVER3*(-ia*sinTable[phaseSG>>8]-ib*sinTable[(phaseSG-21845)>>8]-ic*sinTable[(phaseSG+21845)>>8]);

       //### Control loop ###

       //error calculation
       dpreviouserror=derror;
       qpreviouserror=qerror;

       derror= iRefd-id;// DEBUG = *-1 or +1 ??
       qerror= iRefq-iq;// 23/10/2019 looks like the dq controller is inverted...

       d += kPdq*derror+(kIdq)*dpreviouserror;
       q += kPdq*qerror+(kIdq)*qpreviouserror;
       O += kPdq*(-i0)+(kIdq)*Opreviouserror; // attempt to cancel i0
       // TODO let's see if a specific corrector is needed

       /*//TODO DEBUG
       d=.5;
       q=0.;
       O=0.*/

       // anti wind up
       //The 1.2 limits come from the required value to output a 100% duty cycle after inverse park transform.
       d = (d<-1.2) ? -1.2 : d; // if d<-1.2 then d=-1.2
       d = (d>1.2) ? 1.2 : d; // if d>1.2 then d=1.2
       q = (q<-1.2) ? -1.2 : q; // if d<-1.2 then d=-1.2
       q = (q>1.2) ? 1.2 : q; // if d>1.2 then d=1.2
       //Limits homo-polar variations:
       O = (O<-.1) ? -.1 : O;
       O = (O>.1) ? .1 : O;

       // DEBUG
       //updatePWM(PWMf, .25);
       //updatePWM(PWMe, .05);

       parkinverse(d, q, O, phaseSG);// call reverse Park and update PWM
}



//// #######################################################################
//// # This routine control the input inverter.
//// # A PLL is performed to allow for unit power factor reversible rectifier,
//// # plus DC bus voltage regulation.
//void PFCcontrolEDF(void)
//{
//    /* Rectifier conventions:
//     * ires > 0 => from grid to inverter
//     * vres > 0 => vres // Udc
//     */
//
//    // ### 1- update DC bus voltage controller ###
//
//    //update errors
//    udcpreviouserror = udcerror;
//    udcerror = NOMINAL_U_DC - Vudcmoy;
//
//    //update amplitude controller
//    iRefAmpedf += kPudc*udcerror+ kIudc*udcpreviouserror;// 0.08 -0.07995
//    //Current limitation
//    iRefAmpedf = (iRefAmpedf>IampLimit) ? IampLimit : iRefAmpedf;
//    iRefAmpedf = (iRefAmpedf<-IampLimit) ? -IampLimit : iRefAmpedf;
//    /* due to conventions, iRefAmpedf > 0 means I has the same phase as V,
//     * so the rectifier absorb power, Udc rises.
//     */
//
//    // ### 2- compute a Phase Lock Loop on EDF voltage ###
//    updatePLLedf();
//
//
//
//    // 3- generate input current reference
//    iRefEdf = iRefAmpedf * sinEDF;// + .1 * cosEDF);// a bit in advance
//
//    // 4- update controller for close loop current control
//    /*Control strategy = track current with PI where output PWM ratio edfPwm can be negative.
//     * here the PWM is negative logic:
//     *  If localPWM > 0 then PWMe=1 and PWMd=1-edfPwm, else PWMe=0 and PWMd=-edfPwm,
//     *  therefore Vout = U-V = -localPWM*Udc
//     * dIres/dt = 1/L*(Vedf + localPWM*Udc)
//     * to compensate the effect of Vedf, localPWM is:
//     *  localPWM = edfPwm - Vedf/Udc [Vedf could be modeled by Vres_max*sinEDF
//     *  so dIres/dt = 1/L*(edfPwm*Udc)
//     * PWMe controls V, and PWMd controls U.
//     */
//    float err = iRefEdf-ires;
//    //PWMn = PWMn-1 + P * ERRn + (I*Ts-P) * ERRn-1
//    //TODO !! finir r�glages 0.0700 -0.0698 | 0.0574 -0.0573
//    edfPwm += kPedf*err + kIedf*previousEdfErr; //0.15 -0.147 | .101 -.097
//    previousEdfErr = err;
//    edfPwm = (edfPwm<-2) ? -2 : edfPwm;
//    edfPwm = (edfPwm>2) ? 2 : edfPwm;
//
//    // define the PWM that is applied to the inverter: regulated PWM + compensation
//    //float locaPWM = edfPwm - Vresmax/Vudcmoy*sinEDF; // EDF voltage compensation
//    float locaPWM = edfPwm - .95*Vresmax/Vudcmoy*sinEDF; //+ vres/Vudcmoy;
//
////TODO?    vres=fabsf(vres);
//    //Try: proportional + compensation
//    // pwm = -Vout/Udc + L*Fs/Udc*err
////    edfPwm = -.7*Vresmax/Vudcmoy*sinEDF + kPedf*err;//-.7*Vresmax/Vudcmoy*sinEDF is beatifull
////    edfPwm = 1/Vudcmoy * (-.7*Vresmax*sinEDF + (60 - 4.6*fabsf(iRefEdf))*err);
//
//
////    edfPwm = -.95/Vudcmoy*Vresmax*sinEDF + kPedf*err;
//    //float locaPWM = edfPwm;
//
//    //Saturation + antiwindup
//    locaPWM = (locaPWM<-1) ? -1 : locaPWM;
//    locaPWM = (locaPWM>1) ? 1 : locaPWM;
//
//    // 5- update PWM for EDF inverter
//    if (locaPWM>0) {
//        updatePWM(PWMe, 1);// V on #2
//        updatePWM(PWMd, 1-locaPWM);// U on #2
//        edfPwmState += 1;// V is at Udc
//        if (edfPwmState>2)// a delay is applied, 2 means V is @ Udc
//            edfPwmState=2;
//    } else {
//        updatePWM(PWMe, 0);// V on #2
//        updatePWM(PWMd, -locaPWM);// U on #2
//        edfPwmState -= 1;// V is at GND
//        if (edfPwmState<0)// a delay is applied, 0 means V is @ GND
//            edfPwmState=0;
//    }
//
//
//
//}
//
//
//// #######################################################################
//// # updatePLLedf computes a Phase Lock Loop on EDF voltage
//// # this should be called every PWM interruption, only once!
//// # it uses Vres input to synthesize sinEDF variable that is a uniter
//// # amplitude sine with the same size.
//void updatePLLedf()
//{
//    //compute a Phase Lock Loop on EDF voltage ###
//    //1) evaluate projection signal
//    float NCO_W = NCOedf_cpt + (VALBITMAX/4);// sin(A+90�) = cos(A)
//    if(NCO_W>=VALBITMAX){
//        NCO_W-=VALBITMAX;
//    }
//    cosEDF=sinTable[((uint16_t)NCO_W)>>8]; // projection signal
//
//    //2) compute projected signal
//    //filter: time constant = Tpwm / 0.00005 = 1s??
//    projSinEDF = 0.99995 * projSinEDF + 0.00005 * cosEDF * vres;// see if variable EDF amplitude is a problem
//
//    //3) Loop filter
//    //loopFilterOut = .000164*loopFilterOut + 6.6*projSinEDF;// manual sets
//    //loopFilterOut += -0.039269659490282*loopFilterOut + 0.039269908169872*projSinEDF;// PI for projection filter compensation + 1rad/s bandwidth => a bit slow
//    //loopFilterOut = 20.8*projSinEDF;// P damping @0.7, bandwidth = 1.4rad/s (4s), EDF@230V => in fact, not very stable
//    loopFilterOut = .05*projSinEDF;
//    loopFilterOut = (loopFilterOut > 16) ? 16 : loopFilterOut;// 16 ~increment for 5Hz
//    loopFilterOut = (loopFilterOut < -16) ? -16 : loopFilterOut;
//
//    //NCO
//    NCOedf_cpt += NCO_0 + loopFilterOut;
//    if(NCOedf_cpt>=VALBITMAX){
//        NCOedf_cpt-=VALBITMAX;
//    }
//    sinEDF=sinTable[((uint16_t)NCOedf_cpt)>>8]; // sinus amplitude for reference
//
//    // Debug
//    //GPIO_togglePin(GPIO_DEBUG);
//    if (sinEDF>0)// DEBUG
//       GPIO_writePin(GPIO_DEBUG,1);
//    else GPIO_writePin(GPIO_DEBUG,0);
//
//}

// #######################################################################
// # update_PLL_SG computes a Phase Lock Loop on Smart Grid voltage
// # this should be called every PWM interruption, only once!
// # it uses volatge inputs to evaluate phaseSG
// # TODO /!\ this version only supports increasing phase, so be careful on the wiring!
void update_PLL_SG(void)
{
    //compute a Phase Lock Loop on 3 phase voltages ###

    //1) compute vd
    // vd=ROOT2OVER3*(vAN*cos(phaseSG)+vBN*cos(phaseSG-2pi/3)+vCN*cos(phaseSG+2pi/3));
    float Vd= ROOT2OVER3*(vAN*sinTable[(phaseSG+16384)>>8]+vBN*sinTable[(phaseSG-5461)>>8]+vCN*sinTable[(phaseSG+(uint16_t)38229)>>8]);

    //2) PI controller to cancel vd
    // (error = 0-vd = -vd)
    //OUTn = OUTn-1 + P * ERRn + (I*Ts-P) * ERRn-1
    FreqSG += kPpll3*Vd + kIpll3*previousVdSG;
    previousVdSG = Vd;
    FreqSG = (FreqSG<FreqSG_MIN) ? FreqSG_MIN : FreqSG;
    FreqSG = (FreqSG>FreqSG_MAX) ? FreqSG_MAX : FreqSG;
    //3) update phaseSG
    // PI output works around 50Hz i.e. 164 increments per interruption
    phaseSG += (uint16_t)FreqSG;

    //TODO: test with logh speed log first!
    sinSyncSG = sinTable[(phaseSG)>>8];//synthesize a sin based on this
}

// #######################################################################
// # phaser_SG computes a phaser on Smart Grid voltage
// # this should be called every PWM interruption.
// # it uses voltage inputs to evaluate phaseSG
void phaser_SG(void)
{
    // ### compute the phaser on 3 phase voltages
    // complex formula: phaseSG = angle( vAN + PHa * vBN + PHa� * vCN)

    float realPart = vAN + PHar * vBN + PHar2 * vCN;
    float imagPart =       PHai * vBN + PHai2 * vCN;

    float phase = atan2(imagPart, realPart);

    FreqSG = phase; //DEBUG

    // atan2 returns rad in [-Pi, Pi]. We want integer [0, 2^16] +  2^14 (Pi/2) to get...
    //... a sine in phase with vAN.
    phase = phase*10430.37835 + 16384;// phase*pi/2^15 + 2^14
    phase = (phase < 0) ? 65536. + phase : phase;// 2^16+phase if negative

    // final conversion
    phaseSG = (uint16_t)phase;

    //Synthesize a sin based on this
    sinSyncSG = sinTable[(phaseSG)>>8];
}


