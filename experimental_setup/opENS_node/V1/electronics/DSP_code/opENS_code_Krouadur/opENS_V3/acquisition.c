/*
 * acquisition.c
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      routines for analog acquisitions:
 *          - zeroing and sensors' gains
 *          - statistics:
 *              - averaging/filtering
 *              - RMS� and RMS
 */

#include "acquisition.h"

//
// Variable initializations
//

//### Configuration ###
uint16_t configCalib = CONF_CAL_CurrentZeros;//calibration only perform Current sensors' zeros

//### Average ###
uint16_t adcA0zero=2048;
uint16_t adcA2zero=2048;
uint16_t adcB2zero=2048;
uint16_t adcC2zero=2048;
uint16_t adcA3zero=2048;
uint16_t adcB3zero=0;// vaG sensor starts @0V
uint16_t adcC3zero=0;// vbG sensor starts @0V
uint16_t adcA1zero=0;// vcG sensor starts @0V
uint16_t adcA4zero=0;// Udc sensor starts @0V
uint16_t adcB4zero=2048;

//### Gains of the sensors
// initialized with calibration
float gainIA = CALIBGAINIA;
float gainIB = CALIBGAINIB;
float gainIC = CALIBGAINIC;
float gainIRES = CALIBGAINIRES;
float gainVA = CALIBGAINVA;
float gainVB = CALIBGAINVB;
float gainVC = CALIBGAINVC;
float gainVRES = CALIBGAINVRES;
float gainVUDC = CALIBGAINVUDC;
float gainIA2 = CALIBGAINIA2;


//### SG currents ###
float ia=0, ia2=0;
float ib=0;
float ic=0;
float ires=0;
float vres=0;
float vaG=0, vAN=0; // G: GND, N:NEUTRAL
float vbG=0, vBN=0; // G: GND, N:NEUTRAL
float vcG=0, vCN=0; // G: GND, N:NEUTRAL
float vNG=0,vNGmoy=0, vNGeff=0; // G: GND, N:NEUTRAL
float vudc=0;

//### Measure ###
float Iamoy=0, Iaeff=0;
float Ibmoy=0, Ibeff=0;
float Icmoy=0, Iceff=0;
float Iresmoy=0, Ireseff=0;
float Vamoy=0, Vaeff=0;
float Vbmoy=0, Vbeff=0;
float Vcmoy=0, Vceff=0;
float Vresmoy=0, Vreseff=0;
float Vudcmoy=0, Vresmax=0, Vresmaxmoy=0;
float Ia2moy=0, Ia2eff=0;






//
// #######################################################################
// Function implementations

// #######################################################################
// # This routine updates the measures from the raw samples.
// # The conversion of the ADC and the results must be ready before calling this routine.
// ################################
void sensorAcquisition()
{
    //14/10/2019 change filters
    //Gurvan: 06-28-19 filtering added
    ia= ((float)( (int16_t)adcA0-(int16_t)adcA0zero) ) *gainIA; // Formula : (adc-adczero)/gain
    ib= ((float)( (int16_t)adcA2-(int16_t)adcA2zero) ) *gainIB; // Formula : (adc-adczero)/gain
    ic= ((float)( (int16_t)adcB2-(int16_t)adcB2zero) ) *gainIC; // Formula : (adc-adczero)/gain
    //ic=-ia-ib;
    ires=     ((float)( (int16_t)adcC2-(int16_t)adcC2zero) ) *gainIRES; // Formula : (adc-adczero)/gain

    ia2 = ((float)( (int16_t)adcB4-(int16_t)adcB4zero) ) *gainIA2;

    // Vres sampling : first = sample it, second = compute average, third = Vres value is ample - average
    //vres= 0.1 * (vres + Vresmoy) + 0.9 *
    vres=        ((float)(adcA3) *gainVRES); // Formula : (adc-adczero)/gain
    Vresmoy = 0.9999*Vresmoy + 0.0001*vres;// time constant around 10 grid periods
    vres -= Vresmoy;//compensate sensor offset
    /*if (edfPwmState==0)// compensate the measure due to #2 V state
        vres += Vudcmoy/2;
    else if (edfPwmState==2)
        vres -= Vudcmoy/2;
    else
        vres = 0; // during a transition vres is null*/


    vaG= ((float)( (int16_t)adcB3-(int16_t)adcB3zero) ) *gainVA; // Formula : (adc-adczero)/gain
    vbG= ((float)( (int16_t)adcC3-(int16_t)adcC3zero) ) *gainVB; // Formula : (adc-adczero)/gain
    vcG= ((float)( (int16_t)adcA1-(int16_t)adcA1zero) ) *gainVC; // Formula : (adc-adczero)/gain
    vudc= ((float)( (int16_t)adcA4-(int16_t)adcA4zero) ) *gainVUDC; // Formula : (adc-adczero)/gain
    vNG = 0.33333*(vaG+vbG+vcG);
    vAN = vaG-vNG;
    vBN = vbG-vNG;
    vCN = vcG-vNG;
}


// #######################################################################
// # This routine computes the statistics of the measures.
// # It must be run every 50�s and after sensorAcquisition().
// ################################
void updateStatistics()
{

    //Courants et tensions moyennes et efficaces (RMS�)
    //mean filters:
    Iamoy = COEFF_AV2*Iamoy + COEFF_AV1*ia;
    Iaeff = COEFF_AV2*Iaeff + COEFF_AV1*ia*ia;
    Ibmoy = COEFF_AV2*Ibmoy + COEFF_AV1*ib;
    Ibeff = COEFF_AV2*Ibeff + COEFF_AV1*ib*ib;
    Icmoy = COEFF_AV2*Icmoy + COEFF_AV1*ic;
    Iceff = COEFF_AV2*Iceff + COEFF_AV1*ic*ic;
    Iresmoy = COEFF_AV2*Iresmoy + COEFF_AV1*ires;
    Ireseff = COEFF_AV2*Ireseff + COEFF_AV1*ires*ires;
    //Vresmoy is computed during sampling Vresmoy = 0.9999*Vresmoy + 0.0001*vres;
    Vreseff = COEFF_AV2*Vreseff + COEFF_AV1*vres*vres; // 0.3s time response 95%
    Vamoy = COEFF_AV2*Vamoy + COEFF_AV1*vAN;
    Vaeff = COEFF_AV2*Vaeff + COEFF_AV1*vAN*vAN;
    Vbmoy = COEFF_AV2*Vbmoy + COEFF_AV1*vBN;
    Vbeff = COEFF_AV2*Vbeff + COEFF_AV1*vBN*vBN;
    Vcmoy = COEFF_AV2*Vcmoy + COEFF_AV1*vCN;
    Vceff = COEFF_AV2*Vceff + COEFF_AV1*vCN*vCN;
    Vudcmoy = COEFF_AVDC2*Vudcmoy + COEFF_AVDC1*vudc;// average with tr95% = 0.05s (2.5 grid periods)
    vNGmoy=COEFF_AV2*vNGmoy + COEFF_AV1*vNG;
    vNGeff = COEFF_AV2*vNGeff + COEFF_AV1*vNG*vNG;

    Ia2moy = COEFF_AV2*Ia2moy + COEFF_AV1*ia2;
    Ia2eff = COEFF_AV2*Ia2eff + COEFF_AV1*ia2*ia2;

    // gliding max
    Vresmax -= 0.004;// -0.8V/10ms
    float tmp = (vres > 0) ? vres : -vres;
    Vresmax = (Vresmax < tmp) ? tmp : Vresmax;
    Vresmaxmoy = 0.999*Vresmaxmoy + 0.001*Vresmax;// smooth the max

}



/* ######################################################
 * #  applyCalibration
 * # Use the previously computed statistics to apply calibration to
 * # the sensors.
 * # V3.2.3 calibration can be configured with variable configCalib.
 * ################################
 */
void applyCalibration(void)
{
    // Typecast the zeros to the right type
    if (configCalib & CONF_CAL_CurrentZeros) {
        // Zeros are applied to current sensors.
        adcA0zero = (uint16_t)(Iamoy+2048.);// do not forget to add the previously subtracted 2048
        adcA2zero = (uint16_t)(Ibmoy+2048.);
        adcB2zero = (uint16_t)(Icmoy+2048.);
        adcC2zero = (uint16_t)Iresmoy;
    }

    //adcA3zero = (uint16_t)tmpZero[4]; not used for Vres
    //adcB3zero = (uint16_t)tmpZero[5]; vaG sensor starts @0V
    //adcC3zero = (uint16_t)tmpZero[6]; vbG sensor starts @0V
    //adcA1zero = (uint16_t)tmpZero[7]; vcG sensor starts @0V
    //adcA4zero = (uint16_t)tmpZero[8]; not used for Udc
    //adcB4zero = (uint16_t)Ia2moy; 2020-01-31 let's keep 2048

    if (configCalib & CONF_CAL_VoltageGain) {
        // Set voltage gains
        // We assume the mid voltage is Udc/2, and Udc is accurate.
        // A next step could be to receive the Udc value from an accurate sensor by serial connection.
        // gainVx = Vudcmoy / (2 * VxG_moy) | VxG = VxN + VNG
        // But, just here, VNG is forced to 0, for calibration
        Vudcmoy = (Vudcmoy - 0 ) *gainVUDC;// Conversion to Volts.
        gainVA = Vudcmoy / (2 * (Vamoy));// + vNGmoy));
        gainVB = Vudcmoy / (2 * (Vbmoy));// + vNGmoy));
        gainVC = Vudcmoy / (2 * (Vcmoy));// + vNGmoy));
    }

    if (configCalib & CONF_CAL_CurrentGain) {
        // Set current gains
        /* We assume the voltage gains set, the output capacitor known and the IGBTs are open.
         * Therefore Irms = Vrms.C.w => gainI * (Ieff - Imoy^2)^.5 = gainV * (Veff - Vmoy^2)^.5 * CapaW
         * => gainI = gainV * CapaW * ( (Veff - Vmoy^2) / (Ieff - Imoy^2) )^.5
         * Note 1: it is important the voltages Vx are referenced to Ground (doesn't work
         *         if it was Neutral).
         * Note 2: As we only want the RMS from variations, adding the previously subtracted 2048
         *         to currents is not required.
         */
        gainIA = gainVA * CAPAW_A * sqrtf( (Vaeff - Vamoy*Vamoy)/(Iaeff - Iamoy*Iamoy) );
        gainIB = gainVB * CAPAW_B * sqrtf( (Vbeff - Vbmoy*Vbmoy)/(Ibeff - Ibmoy*Ibmoy) );
        gainIC = gainVC * CAPAW_C * sqrtf( (Vceff - Vcmoy*Vcmoy)/(Iceff - Icmoy*Icmoy) );
    }

    /*gainIRES = CALIBGAINIRES; // reset gain
    gainIA2 = CALIBGAINIA2; // reset gain*/

    // Reset the statistics
    Iamoy=0; Iaeff=0;
    Ibmoy=0; Ibeff=0;
    Icmoy=0; Iceff=0;
    Iresmoy=0; Ireseff=0;
    Vamoy=0; Vaeff=0;
    Vbmoy=0; Vbeff=0;
    Vcmoy=0; Vceff=0;
    Vresmoy=0; Vreseff=0;
    Vudcmoy=0; Vresmax=0; Vresmaxmoy=0;
    Ia2moy=0; Ia2eff=0;
}

