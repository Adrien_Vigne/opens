// Configuration for opENS A3
// second prototype VprotoElec1 "Glazig"

#ifndef CONFIG_ID

// ID name of the hardware
#define CONFIG_ID "A3"
#define CALIBGAINIA 0.00722 //2019-11-04
#define CALIBGAINIB 0.00645 //2019-11-04
#define CALIBGAINIC 0.00690 //2019-11-04
#define CALIBGAINIRES 0.0061
#define CALIBGAINVA 0.099
#define CALIBGAINVB 0.099
#define CALIBGAINVC 0.099
#define CALIBGAINVRES 0.178
#define CALIBGAINVUDC 0.0904 //2019-10-31

#define CALIBGAINIA2 0.029296875 // Amp/count LEM LTS 25-NP + Rdiv 3/5 - 2020-01-31

//Partie mise � jour par Rachel Neveu // 24/07/2020
//Output capacitor (F) x pulsation (2pi 50Hz)
#define CAPAW_A 314.1*8.6214e-6 //2020-01-28
#define CAPAW_B 314.1*8.6577e-6 //2020-01-28
#define CAPAW_C 314.1*8.5564e-6 //2020-01-28



#endif
