/*
 * Gus 04-03-2019
 * from adc_soc_software_sync_cpu01.c
 *
 * ADC Synchronous SOC Software Force (adc_soc_software_sync)
 *
 *  This example converts some voltages on ADCA and ADCB using input 5 of the
 *  input X-BAR as a software force. Input 5 is triggered by toggling GPIO0,
 *  but any spare GPIO could be used. This method will ensure that both ADCs
 *  start converting at exactly the same time.
 *
 *  2020-01-20 Adding adcB4
 *
 */

#ifndef MYADC_H_
#define MYADC_H_


#include "device.h"

// settings
#define EX_ADC_RESOLUTION       12 // set 12 bit resolution


// Global variables
extern uint16_t adcA0;
extern uint16_t adcA2;
extern uint16_t adcB2;
extern uint16_t adcC2;
extern uint16_t adcA3;
extern uint16_t adcB3;
extern uint16_t adcC3;
extern uint16_t adcA1;
extern uint16_t adcA4;
extern uint16_t adcB4;

// Function Prototypes
//
void initADCs(void); // start the hardware
void initADCSOCs(void); // configure the SOCs to software triggered sample on defined pins
void startConvADC(void); // start a conversion
bool ADCresultsReady(void); // send true once the results are available and stored in the adcXi variables


#endif /* MYADC_H_ */
