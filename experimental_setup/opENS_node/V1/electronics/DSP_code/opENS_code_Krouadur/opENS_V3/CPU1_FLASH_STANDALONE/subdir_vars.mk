################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../2837xD_FLASH_lnk_cpu1.cmd 

LIB_SRCS += \
../driverlib.lib 

C_SRCS += \
../V3.2.c \
../acquisition.c \
../myADC.c \
../myDebugTools.c \
../myPWM.c \
../myUSBserial.c \
../myheader.c \
../parkcontrol.c \
../remote_vars.c \
../statemachine.c 

C_DEPS += \
./V3.2.d \
./acquisition.d \
./myADC.d \
./myDebugTools.d \
./myPWM.d \
./myUSBserial.d \
./myheader.d \
./parkcontrol.d \
./remote_vars.d \
./statemachine.d 

OBJS += \
./V3.2.obj \
./acquisition.obj \
./myADC.obj \
./myDebugTools.obj \
./myPWM.obj \
./myUSBserial.obj \
./myheader.obj \
./parkcontrol.obj \
./remote_vars.obj \
./statemachine.obj 

OBJS__QUOTED += \
"V3.2.obj" \
"acquisition.obj" \
"myADC.obj" \
"myDebugTools.obj" \
"myPWM.obj" \
"myUSBserial.obj" \
"myheader.obj" \
"parkcontrol.obj" \
"remote_vars.obj" \
"statemachine.obj" 

C_DEPS__QUOTED += \
"V3.2.d" \
"acquisition.d" \
"myADC.d" \
"myDebugTools.d" \
"myPWM.d" \
"myUSBserial.d" \
"myheader.d" \
"parkcontrol.d" \
"remote_vars.d" \
"statemachine.d" 

C_SRCS__QUOTED += \
"../V3.2.c" \
"../acquisition.c" \
"../myADC.c" \
"../myDebugTools.c" \
"../myPWM.c" \
"../myUSBserial.c" \
"../myheader.c" \
"../parkcontrol.c" \
"../remote_vars.c" \
"../statemachine.c" 


