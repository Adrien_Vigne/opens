/*
 * parkcontrol.h
 *
 *  Created on: 22 janv. 2020
 *      Author: Chercheur
 *
  *      All the routines for d-q park control
 */

#ifndef PARKCONTROL_H_
#define PARKCONTROL_H_

#include "myheader.h"

//
// Function Prototypes
//
void parkinverse(float, float, float, uint16_t);
void DQcontrolSG(void);
//void PFCcontrolEDF(void);
//void updatePLLedf(void);
void update_PLL_SG(void);
void phaser_SG(void);

// Variables and constants declaration

//### Phase of SG ###
extern uint16_t phaseSG;// phase counter, use sinTable[phase>>8], i.e. phase from 0 to 256*2^8=65535

//### 3 phase PLL parameters
#define refFreqSG 164 //frequence corresponding to 50Hz (in increments per interrupt)
extern float previousVdSG;//memory of previous D voltage in DQ plan, for 3 phase PLL control
extern float FreqSG;
#define kPpll3 1 // this corrector works for amplitude voltages over 5V, which is always the case.
#define kIpll3 (1*Ts-kPpll3)
#define FreqSG_MAX 181 // ~44Hz
#define FreqSG_MIN 145 // ~55Hz
extern float sinSyncSG;

//### phaser parameters
// PHa = exp(j*2*Pi/3) = PHar + j*PHai. PHar2 and PHai2 stands for PHa�
#define PHar -.5
#define PHai 0.866025403784439
#define PHar2 -.5
#define PHai2 -0.866025403784439


//### Park transform variables ###
extern float iRefq;
extern float iRefd;
extern float d;
extern float q;
extern float O;
extern float id;
extern float iq;
extern float i0;
extern float vd;
extern float vq;
extern float v0;

extern float derror;
extern float qerror;
//extern float Oerror; unused
extern float dpreviouserror;
extern float qpreviouserror;
extern float Opreviouserror;

//abc control
extern float ia_ref;
extern float ib_ref;
extern float ic_ref;
extern float a;
extern float b;
extern float c;
extern float a_error;
extern float b_error;
extern float c_error;
extern float a_previouserror;
extern float b_previouserror;
extern float c_previouserror;

extern float kPdq;// TODO set params, use #define when done
extern float kIdq;
extern float kP0;// TODO set params, use #define when done
extern float kI0;

extern float DEBUGprobeVar1; //debug variable
extern float DEBUGprobeVar2; //debug variable
extern float DEBUGprobeVar3; //debug variable

/*//### EDF rectifier control ###
float udcerror=0, udcpreviouserror=0;// Udc PI controller
#define kPudc 1
#define kIudc -.99995
float iRefAmpedf=0, iRefEdf=0; // amplitude of the reference EDF current, reference EDF current
float previousInputSignEDF=0;
float sinEDF=0;// sinus envelop to generate current reference (= output of PLL)
float cosEDF=0;// cosEDF is the projection signal, i.e. 90� phase in advance compared to sinEDF
float projSinEDF=0;// projected signal
float loopFilterOut=0, previous1loopFilterOut=0;
float NCOedf_cpt=0; // phase counter for Numerical Controlled Oscillator
#define NCO_0  163.84 // valeur pour osciller � fc_vco
//Current controller
float edfPwm=0, previousEdfErr=0;// Ires PI controller
#define kPedf 0.024127 // 2019-10-17
#define kIedf -0.024117
int16_t edfPwmState=0; // state of #2 V, to compensate vres measure.
float IampLimit = 0; // current amplitude limitation*/










#endif /* PARKCONTROL_H_ */
