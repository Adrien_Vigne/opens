/*
 * statemachine.c
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      Contains the routines for state machine control.
 *
 */


#include "statemachine.h"

//
// Global initialization
//
uint16_t MainState=START_UP; // Initial state
int32_t compt=TIME_START_UP; // Initial state duration

// Nothing more here so far, as all the important state machine code is in the main file.

