/*
 * myUSBserial.c
 *
 *  Created on: 6 mars 2019
 *      Author: Gurvan
 *
 *      Gus 06-03-2019
 *
 *      Julien 25-06-2019
 *      Add fonction ftoa to convert a float into characters for the USBserial
 *      Change itoa to convert a int instead of uint
 *
 *      Gus 06-28-2019
 *      Fix bug about length not found
 *
 *      from sci_ex3_echoback.c
 *
 *      16-10-2019 change pow to fpown to be compatible with "math.h"
 *      21-08-2020 add atof, atoi and getString
 */

#include "myUSBserial.h"
#include <string.h>

bool sign=false;

// ### initUSBserial #############################################
//
void initUSBserial()
{
    // ######### GUS route to USB
    // GPIO43 is the SCI Rx pin from USB.
    //
    GPIO_setMasterCore(43, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_43_SCIRXDA);
    GPIO_setDirectionMode(43, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(43, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(43, GPIO_QUAL_ASYNC);

    //
    // GPIO42 is the SCI Tx pin from USB.
    //
    GPIO_setMasterCore(42, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_42_SCITXDA);
    GPIO_setDirectionMode(42, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(42, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(42, GPIO_QUAL_ASYNC);

    //
    // Initialize SCIA and its FIFO.
    //
    SCI_performSoftwareReset(SCIA_BASE);

    //
    // Configure SCIA.
    //  8bit-1stop-noParity

    SCI_setConfig(SCIA_BASE, my_LSPCLK_FREQ, myBaudRate, (SCI_CONFIG_WLEN_8 |
                                                        SCI_CONFIG_STOP_ONE |
                                                        SCI_CONFIG_PAR_NONE));
    SCI_resetChannels(SCIA_BASE);
    SCI_resetRxFIFO(SCIA_BASE);
    SCI_resetTxFIFO(SCIA_BASE);
    SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_TXFF | SCI_INT_RXFF);
    SCI_enableFIFO(SCIA_BASE);
    SCI_enableModule(SCIA_BASE);
    SCI_performSoftwareReset(SCIA_BASE);

}

// ### USBserialSend #############################################
// msg must finish by \0 and its length should not exceed 128 chars.
void USBserialSend(unsigned char *msg) // send a message through serial USB
{

    uint16_t* uintmsg=(uint16_t*)msg;

    // find length
    /*//loop until char /0 is found
    uint16_t len;
    for(len=0; len<=255; len++)
    {
        if (uintmsg[len]==0)
            break;
    }*/
    uint16_t len;
    len = (uint16_t)strlen(msg);
    // check if last char is \0
    if (uintmsg[len] != 0)
        ESTOP0; // E-stop! TODO do a better error management, i.e. add the \0 char at the end.

    len++;// length is table position + 1, as the position starts by 0 in C


    // send message through serial
    SCI_writeCharArray(SCIA_BASE, (uint16_t*)msg, len);

}


// ### itoa ################################################
// convert Integer To Output String
void itoa(int32_t value, unsigned char* result, uint16_t base)
{
  // check that the base if valid
  if (base < 2 || base > 36) { *result = '\0';}

  unsigned char* ptr = result, *ptr1 = result, tmp_char;
  int32_t tmp_value;
  bool tmp=false;

  if(value<0){
      tmp=true;
      value*=-1;
  }

  do {
    tmp_value = value;
    value /= base;
    *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
  } while ( value );

  // Apply negative sign
  if (tmp || sign) *ptr++ = '-';
  *ptr-- = '\0';
  while(ptr1 < ptr) {
    tmp_char = *ptr;
    *ptr--= *ptr1;
    *ptr1++ = tmp_char;
  }
  sign=false;
//  asm("   ESTOP0");
}

// ### ftoa ################################################
// Converts a floating point number n into a string with a
// precision after the decimal point of afterpoint
void ftoa(float n, unsigned char* res, uint16_t afterpoint)
{
    uint16_t j=0;
    // Extract integer part
    if(n<0.0) sign=true;
    int32_t ipart = (int32_t)n;


    // Extract floating part
    float fpart = n - (float)ipart;
    if(fpart<0.0) fpart*=-1;

    // convert integer part to string
    itoa(ipart, res, 10);
    uint16_t i=(uint16_t)strlen(res);

    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.';  // add dot

        while(fpart<0.1  && j<afterpoint){ // Gus: this doesn't work "&& fpart!=0.0"
            fpart*=10;
            i++;
            res[i] = '0';
            j++;
        }
        fpart = fpart * fpown(10, afterpoint-j);
        itoa((int32_t)fpart, res + i + 1, 10);
    }
}

// #######################################################################
// Power function
float fpown(float x, uint16_t n){
    if(n==0){
        return 1;
    }
    else{
        return x*fpown(x,n-1);
    }
}


// #######################################################################
// atof function: string to float
float  atof(char s[])
{
    float val, power;
    int16_t i, sign;


    for (i = 0; isspace(s[i]); i++) /* skip white space */
        ;
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-')
        i++;
    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if (s[i] == '.')
        i++;
    for (power = 1.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
        power *= 10;
    }
    return sign * val / power;
}



// #######################################################################
// atoi function: string to integer
int16_t  atoi(char s[])
{
    int16_t val;
    int16_t i, sign;


    for (i = 0; isspace(s[i]); i++) /* skip white space */
        ;
    sign = (s[i] == '-') ? -1 : 1;// see if + or -
    if (s[i] == '+' || s[i] == '-')// if we have a sign, we skip it.
        i++;
    for (val = 0; isdigit(s[i]); i++)
        val = 10 * val + (s[i] - '0');
    return sign * val;
}


// ### getString ################################################
// gather chars from serial and store a string in res. It stops when
// maxN chars are stored or when stopChar is received. res is \0 elsewhere.
//
// /!\ use stopping reading from serial, this function should be interruptible.
void getString(uint16_t maxN, char stopChar, unsigned char* res)
{
    uint16_t i;

    // clear res
    for (i=1; i<maxN; i++)//start from 1 because next step overwrite it.
        res[i]="\0";

    for (i=0; i<maxN; i++) {// receive up to maxN chars
        res[i]=(char)SCI_readCharBlockingFIFO(SCIA_BASE);// wait for a char and store it

        if (res[i]==stopChar) //check if we receive a stopChar
            break;
    }



}

