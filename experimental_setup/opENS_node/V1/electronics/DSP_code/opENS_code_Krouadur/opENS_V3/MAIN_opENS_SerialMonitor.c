/*  Julien
 *  05-28-19
 *  opENS serial-PWM-ADC V1.0
 *
 *  Test cartes puissances et cartes de mesures
 *  PWM triangle
 */


//
// Included Files
//
#include "driverlib.h"
#include "device.h"

#include "myADC.h" // to manage the Analog->Digital converters in a synchronous way
#include "myPWM.h" // to manage the PWM
#include "myUSBserial.h" // to manage the serial connection through USB

// Local routines
int16_t getNumberFromKeyboard(void);

//
// Main
//
void main(void)
{
    // ###############################################################
    // ### Initialization

    uint16_t receivedChar;// contains the incoming characters from the USB serial
    unsigned char* tmpString;// contains manipulated string to be output to USB serial
    uint16_t tmp;// temporary variable

    // Initialize device clock and peripherals
    Device_init(); // initial configuration

    // ### I/O pin settings
    //
    // Initialize GPIO and configure the GPIO pin as a push-pull output
    Device_initGPIO();
    GPIO_setPadConfig(DEVICE_GPIO_PIN_LED1, GPIO_PIN_TYPE_STD); // to blink the LED
    GPIO_setDirectionMode(DEVICE_GPIO_PIN_LED1, GPIO_DIR_MODE_OUT);

    // ### USB serial connection settings
    //
    initUSBserial();


    // ### Interruptions
    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    Interrupt_initModule();
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    Interrupt_initVectorTable();

    // ### ADC setup
    initADCs();// power up the hardware
    initADCSOCs();// set software trigger

    // ### PWM setup
    initPWMs(); // initialize the PWM 1,2 and 3 with the corresponding pins


    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    EINT;
    ERTM;


    // ################################################################
    // main program

    // Welcome message
    USBserialSend("opENS V1.1\r\n\0");
    USBserialSend("G. Jodin 2019\r\n\0");
    USBserialSend("Analog in, PWM out serial interface\r\n\0");
    USBserialSend("\r\nInstructions: send the following letters to...\r\n\0");
    USBserialSend("i : display analog inputs\r\n\0");
    USBserialSend("p : display PWM setting\r\n\0");
    USBserialSend("a, b, c, d, e, f : change corresponding PWM duty cycle ...\r\n\0");
    USBserialSend("                   ... Duty cycle is asked after in percents (0 to 100)\r\n\0");


    // ### PWM
    startPWM();// start the PWM

    // Main forever loop
    for(;;)
    {
        USBserialSend("->\0"); // send an arrow to say ready to hear from you
        //
        // Read a character from the FIFO.
        //
        receivedChar = SCI_readCharBlockingFIFO(SCIA_BASE);
        // send it back + go to new line
        SCI_writeCharBlockingFIFO(SCIA_BASE, receivedChar);
        USBserialSend("\r\n\0");

        switch (receivedChar)
        {
        case 'i' : // #####################################################
            // ######## Analog inputs

                USBserialSend("Analog input states:\r\n\0");
                // ### Sample values on pins A0, A1,B0 and B1
                startConvADC(); // start the conversion
                while (ADCresultsReady())
                   {// do nothing
                   }
                // Conversion done, results are saved in variables adcA0, adcA2, adcB2, adcC2, adcA3, adcB3, adcC3.
                // adcA0
                itoa(adcA0, tmpString, 10);
                USBserialSend(" M_IA   = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcA2
                itoa(adcA2, tmpString, 10);
                USBserialSend(" M_IB   = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcB2
                itoa(adcB2, tmpString, 10);
                USBserialSend(" M_IC   = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcC2
                itoa(adcC2, tmpString, 10);
                USBserialSend(" M_IRES = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcA3
                itoa(adcA3, tmpString, 10);
                USBserialSend(" M_VRES = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcB3
                itoa(adcB3, tmpString, 10);
                USBserialSend(" M_VA   = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcC3
                itoa(adcC3, tmpString, 10);
                USBserialSend(" M_VB   = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcA1
                itoa(adcA1, tmpString, 10);
                USBserialSend(" M_VC   = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");
                // adcA4
                itoa(adcA4, tmpString, 10);
                USBserialSend(" M_UDC  = ");
                USBserialSend(tmpString);
                USBserialSend("\r\n\0");

                break;
        case 'p' : // #####################################################
            // ######## display PWM setting
                USBserialSend("PWM states:\r\n\0");
                // ### Counter is reversed => 1250 correspond to 0% PWM and 0 correspond to 100% PWM ###

                tmp = EPWM_getCounterCompareValue(EPWM1_BASE,
                                            EPWM_COUNTER_COMPARE_A); // PWM 1A
                itoa(tmp*0.08, tmpString, 10); // itoa(tmp/12.5, tmpString, 10); because counter is included in [0;1250]
                USBserialSend("a PWM 1A = ");
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                tmp = EPWM_getCounterCompareValue(EPWM2_BASE,
                                            EPWM_COUNTER_COMPARE_A); // PWM 2A
                itoa(tmp*0.08, tmpString, 10); // itoa(tmp/12.5, tmpString, 10); because counter is included in [0;1250]
                USBserialSend("b PWM 2A = ");
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                tmp = EPWM_getCounterCompareValue(EPWM3_BASE,
                                            EPWM_COUNTER_COMPARE_A); // PWM 3A
                itoa(tmp*0.08, tmpString, 10); // itoa(tmp/12.5, tmpString, 10); because counter is included in [0;1250]
                USBserialSend("c PWM 3A = ");
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                tmp = EPWM_getCounterCompareValue(EPWM1_BASE,
                                            EPWM_COUNTER_COMPARE_B); // PWM 1B
                itoa(tmp*0.08, tmpString, 10); // itoa(tmp/12.5, tmpString, 10); because counter is included in [0;1250]
                USBserialSend("d PWM 1B = ");
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                tmp = EPWM_getCounterCompareValue(EPWM2_BASE,
                                            EPWM_COUNTER_COMPARE_B); // PWM 2B
                itoa(tmp*0.08, tmpString, 10); // itoa(tmp/12.5, tmpString, 10); because counter is included in [0;1250]
                USBserialSend("e PWM 2B = ");
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                tmp = EPWM_getCounterCompareValue(EPWM3_BASE,
                                            EPWM_COUNTER_COMPARE_B); // PWM 3B
                itoa(tmp*0.08, tmpString, 10); // itoa(tmp/12.5, tmpString, 10); because counter is included in [0;1250]
                USBserialSend("f PWM 3B = ");
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
        case 'a' : // #####################################################
            // ######## display PWM setting
                USBserialSend("a PWM 1A new value in percent: -> \0");

                tmp = getNumberFromKeyboard();

                updatePWM(PWMa, (float)tmp*.01); // change PWM ratio

                USBserialSend("==> set to ");
                itoa(tmp, tmpString, 10);
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
        case 'b' : // #####################################################
            // ######## display PWM setting
                USBserialSend("b PWM 2A new value in percent: -> \0");

                tmp = getNumberFromKeyboard();

                updatePWM(PWMb, (float)tmp*.01); // change PWM ratio

                USBserialSend("==> set to ");
                itoa(tmp, tmpString, 10);
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
        case 'c' : // #####################################################
            // ######## display PWM setting
                USBserialSend("c PWM 3A new value in percent: -> \0");

                tmp = getNumberFromKeyboard();

                updatePWM(PWMc, (float)tmp*.01); // change PWM ratio

                USBserialSend("==> set to ");
                itoa(tmp, tmpString, 10);
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
        case 'd' : // #####################################################
            // ######## display PWM setting
                USBserialSend("d PWM 1B new value in percent: -> \0");

                tmp = getNumberFromKeyboard();

                updatePWM(PWMd, (float)tmp*.01); // change PWM ratio

                USBserialSend("==> set to ");
                itoa(tmp, tmpString, 10);
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
        case 'e' : // #####################################################
            // ######## display PWM setting
                USBserialSend("a PWM 2B new value in percent: -> \0");

                tmp = getNumberFromKeyboard();

                updatePWM(PWMe, (float)tmp*.01); // change PWM ratio

                USBserialSend("==> set to ");
                itoa(tmp, tmpString, 10);
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
        case 'f' : // #####################################################
            // ######## display PWM setting
                USBserialSend("f PWM 3B new value in percent: -> \0");

                tmp = getNumberFromKeyboard();

                updatePWM(PWMf, (float)tmp*.01); // change PWM ratio

                USBserialSend("==> set to ");
                itoa(tmp, tmpString, 10);
                USBserialSend(tmpString);
                USBserialSend("%\r\n\0");

                break;
       default :// ############################################
            // ###### other keys
                USBserialSend("Incorrect command!\r\n\0");// incorrect command
        }
    }
}




// #######################################################################
// ### getNumberFromKeyboard()
// this routine manage the user to enter a number from 0 to 100
//
int16_t getNumberFromKeyboard(){
    int16_t i;
    int16_t tmp=0;// initialize value
    uint16_t receivedChar;

    for (i=0; i<3; i++){// wait for up to 3 digits

        receivedChar = 0;

        // loop until return or a digit comes in
        while ( !(receivedChar == 13U || (receivedChar >= '0' && receivedChar <= '9')) ) {
            // wait for numeric or return char
            receivedChar = SCI_readCharBlockingFIFO(SCIA_BASE);
        }

        if (receivedChar == 13) // hit return (touche entrée)
            break;

        SCI_writeCharBlockingFIFO(SCIA_BASE, receivedChar); // send back the char to display it

        // add the digit value
        tmp = tmp*10 + receivedChar-'0';

    }

    // check range
    tmp = (tmp>100) ? 100 : tmp;
    //tmp = (tmp<0) ? 0 : tmp; useless because integer is always positive.
    return tmp;
}





//
// End of File
//
