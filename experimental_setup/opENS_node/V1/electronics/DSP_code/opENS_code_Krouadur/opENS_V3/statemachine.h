/*
 * statemachine.h
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      Contains the routines for state machine control.
 *
 */

#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_

#include "myheader.h"

//
// Definition of the states
// /!\ Do not change the numbers of the first bloc!!!
// /!\ We may add news steps but not change them, or the serial interface will be broken!
#define START_UP        0
#define IDLE_STATE      1 // Idle state
#define CALIB_SENSOR    2
#define STARTPFC        3
#define PARKCONTROL     4
#define OPENLOOP        5

#define START_UP2       6
#define CALIB_SENSOR2   7
#define STARTPFC2       8
#define STARTPFC3       9

// Definition of durations for states
// counter time by high speed calls (x50�s or /20kHz)
#define TIME_START_UP 100000 // 5s
#define TIME_CALIB_SENSOR 120000 // 6s
#define TIME_STARTPFC 20000 // 5s


// Globals
extern uint16_t MainState;
extern int32_t compt;

#endif /* STATEMACHINE_H_ */
