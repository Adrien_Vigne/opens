/*
 * from Gurvan 20-01-20
 * contributors:
 *      Gurvan Jodin 2019-2020
 *      Julien Leyrit 2019
 *  opENS V3
 *
 *  ...
 *      V2.4.1
 *          13-01-2020
 *          from V3.2, but remove EDF control.
 *          This program is for opENS nodes directly supplied on Udc.
 *
 *          17-01-2020
 *          adding ia2 on adcB4, through LEM
 *
 *      V3.0.1
 *          22-01-2020
 *          cleaning the files: rearrange the functions in different files
 *              ok and works fine!
 *
 *      V3.0.2
 *         28-01-2020
 *          calibration is performed around mid ADC voltages instead
 *          of 0, to enhance accuracy due to float limitation.
 *         31-01-2020
 *          change ia2 gain because I rediscovered that ADC = 4096 when Vin = 3V, not 3V3...
 *          ia2 is still a shit...
 *
 *      V3.0.3
 *         03-02-2020
 *          set dq control, with pahser.
 *          /!\ Optimizer disabled, as it stored some variable into FLASH memory that cannot be further written.
 *          implementation of i0 cancellation by homo-polar control
 *
 *      V3.1
 *         06-07-2020
 *         add an open loop functionality for testing the node. Be careful not to breack everything!
 *         It is called using the character # in serial input.
 *         30/07/2020
 *         updated with Rachel's config files
 *
 *      V3.1.1
 *          21-08-2020
 *          adding serial get/set memory
 *              1- add atoi and atof = DONE
 *              2- set a table of accessible variable = DONE
 *              3- code the Version function, ex. "V", then send back "v"+VERSION+" "remoteVarVersion + \r = DONE
 *              4- code the Get function, ex. "G 12\0" to receive the 12th variable in serial + \r = DONE
 *              5- code the Set function, ex. "S 12 3.14\0" to set the 12th variable to 3.14 = DONE
 *              6- code the Reset function = DONE
 *              7- test all of this a lot
 *              8- change how we switch state: manage the timer automatically so that just writing state is enough (add some initialization states) = DONE
 *
 *      V3.2
 *          24-08-2020
 *          better close loop control
 *          1- compensate voltage measure in dq0    - DONE, doesn't work
 *          2- tune corrector                       - DONE, doesn't work, why do I have instabilities at lower Rserial?
 *          3- if it doesn't work, try with abc control - DONE, it's worse
 *
 *      V3.2.1
 *          2020-08-25
 *              High speed logs configurable with share vars = OK
 *      V3.2.2
 *          2020-08-25
 *          decrease open loop # amplitude to allow control by other nodes DONE
 *
 *          2020-08-26
 *          try directly on grid instead of another node
 *          see biblio to find control algorithm (PhD thesis of Guuillaume x. or Gijon paper)
 *
 *      V3.2.3
 *          2020-08-28
 *          try on "infinite grid" with other table + transfo
 *              -> calib is tricky: floating voltage => currents not balance so zero curent should be in a separate manip
 *              -> as voltages not centered, delete voltage calib!
 *          Solutions:
 *              1- remove voltage sensor calibration    DONE
 *              2- separate calib in different calibs: calib zero only, calib amplitude no zero, calib all.
 *                  variable uint16_t configCalib = 0b...ab, where a=1 means calib currents' zeros, b=1 means calib currents' gains
 *
 *           Avec tout le monde, le soir
 *              repassé en dq classic, en adaptant le gain proportionnel uniquement, on trouve en fonction du courant un truc qui marche pas mal
 *              dégager pour voir les capa des filtres des capteurs de courant
 *                  gain @faibles valeurs : kP = 0.242749   kI = -0.242349, on enlève 5% de P 3-4 fois à 2A et encore 4-6 fois à 4A
 *              on est monté à 4A Id
 *            Remettre Id bien aligné (ici c'est décallé de Iq)
 *
 *            2020-08-31
 *            résistances de filtre enlevé : marche plus du tout -> niveau de bruit sur courants est haut, diaphonie visible
 *
 *            2020-09-16
 *            test avec filtres changés : capa de 3.3nF
 *            fonctionne très bien avec gain kP = 0.065020  kI = -0.064912
 *
 *       V3.2.4 new gains
 *       V3.2.5 fix dq orientations
 *       V3.2.6 add dq0 measures in open loop
 *       V3.2.7 fix homopolar controller
 *       V3.3 dq0: other gains for I0
 *
 * TODO: try other gains
 *
 */


//
// Definitions
//

#define localVERSION "." //local suffix to check at the beginning in serial, for debug purpose.

//
// Included Files
//

//
// opENS node configuration file is defined in the header
//
#include "myheader.h" // Contains all the inclusions, declaration and definitions


//########################################

// Main
void main(void)
{

    // ###############################################################
    // ### Initialization functions
    // Initialize and configure the device, GPIO, interrupts, timers, SPI, PWM, ADC
    myInitialization();

    // ################################################################
    // main program

    // Welcome message
    USBserialSend("opENS_V"
            VERSION localVERSION
            "\r\n"
            CONFIG_ID
            "\r\nGus 2020\r\n\0");
    GPIO_writePin(DEVICE_GPIO_PIN_LED1,1);// switch on the LED

    // Main forever loop
    for(;;)
    {
        // Check if something arrives from serial connection
        if (SCI_getRxFIFOStatus(SCIA_BASE) != SCI_FIFO_RX0) // if RX buffer not empty
        {
            uint16_t receivedChar = SCI_readCharNonBlocking(SCIA_BASE);// read the received char

            if (!ManageRemoteVarsSerial(receivedChar)){// call first Remote Vars routine
                // if it ain't Remote Vars, call the routine to manage debug serial interface.
                LowPriorityLoop_KeyboardInterface(receivedChar);
                }


        }

        // Manage the logs through serial connection
        LowPriorityLoop_logs();


    }
}

// ###########################################################################################################################################
// Main interrupt every 50�s (20kHz)
//
interrupt void HighSpeed_interrupt(void)
    {

    // Start measures
    startConvADC(); // start the conversion


    //Manage the records for high speed log
    HighSpeedLogs();


    while (ADCresultsReady())// wait for new samples
         {// do nothing
         }


    // #######################################################################################################################################
    // State machine
    switch(MainState)
    {
    /*##########################
    ### Starting up
    ############################*/
    case START_UP : // initialize the state
        {
            MainState=START_UP2; // Initial state
            compt=TIME_START_UP; // Initial state duration
            break;
        }
    case START_UP2 : // wait for 5s
        {
            compt--;
            GPIO_writePin(DEVICE_GPIO_PIN_LED1,1);// switch off the LED
            if (compt<=0) //time to go next
            {
                MainState = IDLE_STATE; // let's do nothing after this
            }

            break;
        }
    /*##########################
    ### Sensor calibration
    ############################*/
    case CALIB_SENSOR : // sensor calibration part #1
        {
            // Reset the statistics, gains and zeros.
            Iamoy=0; Iaeff=0;// gainIA=1; adcA0zero=2048;
            Ibmoy=0; Ibeff=0;// gainIB=1; adcA2zero=2048;
            Icmoy=0; Iceff=0;// gainIC=1; adcB2zero=2048;
            Iresmoy=0; Ireseff=0;// gainIRES=1; adcC2zero=0;
            Vamoy=0; Vaeff=0;
            Vbmoy=0; Vbeff=0;
            Vcmoy=0; Vceff=0;
            Vresmoy=0; Vreseff=0;
            Vudcmoy=0; Vresmax=0; Vresmaxmoy=0;

            Ia2moy=0; Ia2eff=0;// gainIA2=1; adcB4zero=2048;

            compt = TIME_CALIB_SENSOR;// initialize counter

            MainState = CALIB_SENSOR2; // Let's go directly to next step

            break;
        }
    case CALIB_SENSOR2 : // sensor calibration part #2
        {

            //Blink the LED at 2.44Hz
            if ( (compt & 8191)==0x1 )// this event occurs every 0.4096s
                GPIO_togglePin(DEVICE_GPIO_PIN_LED1);

            // Sensor acquisition
            /* The idea is to use the existing routine updateStatistics().
             * This routine computes AVERAGE and RMS� of the sensors.
             * Usually, we use the sensorAcquisition() routine to get the ADC values, but
             * it provides the values with the gains and zero. As we want raw ADCs values,
             * we perform direct acquisition.
             * Plus, we force vNG = 0 to ensure the voltages' statistics are relative to
             * ground voltage.
             */
            ia = ((float)adcA0) - 2048.;// currents are referenced to halve span to ...
            ib = ((float)adcA2) - 2048.;//  ensure accuracy of average square values ...
            ic = ((float)adcB2) - 2048.;//  due to signle precision float limitation.
            ires = (float)adcC2;
            ia2 = (float)adcB4;
            vres = (float)adcA3;
            vaG = (float)adcB3;
            vbG = (float)adcC3;
            vcG = (float)adcA1;
            vudc = (float)adcA4;
            /* Statistics are computed on vXN, but vXG are required for calibration.
             * We force vNG = 0 (i.e. Neutral = Ground, even if it is wrong.)
             * As the amplitudes of voltages are large compared to the average, good
             * quality of RMS is ensured even even without measuring around half range.
             */
            vNG = 0;//0.33333*(vaG+vbG+vcG);
            vAN = vaG-vNG;
            vBN = vbG-vNG;
            vCN = vcG-vNG;

            if(compt<=0){ // Is calibration done ?

                // Use the tmpZero[] to apply calibration
                applyCalibration();

                GPIO_writePin(DEVICE_GPIO_PIN_LED1,0);// switch on the LED
                // go to idle state
                MainState=IDLE_STATE;
                break;
            }

            compt--; // time is running out!

            break;
        }
    /*##########################
    ### Start Smart Grid power
    ############################*/
    case STARTPFC : // Start the PLL or phaser
        {
            MainState = STARTPFC2;
            compt = TIME_STARTPFC;
            break;
        }
    case STARTPFC2 : // Start the PLL or phaser
        {
            //Blink the LED at 2.44Hz
            if ( (compt & 8191)==0x1 )// this event occurs every 0.4096s
                GPIO_togglePin(DEVICE_GPIO_PIN_LED1);

            // Sensor acquisition
            sensorAcquisition();

            //Get the phase of Smart Grid
            phaser_SG();
            //update_PLL_SG();

            //State evolution
            compt--;
            if(compt<=0) // Timer done ?
            {
                // DQ control of Smart Grid, set to 0
                iRefq = 0;
                iRefd = 0;

                MainState=STARTPFC3;
                compt = TIME_STARTPFC;// reset counter
            }

            break;
        }
    case STARTPFC3 : // Start the PWM with zero current reference.
        {

            //PWM watchdog service to enable PWM
            GPIO_togglePin(GPIO_WC);

            //Blink the LED at 2.44Hz
            if ( (compt & 8191)==0x1 )// this event occurs every 0.4096s
                GPIO_togglePin(DEVICE_GPIO_PIN_LED1);

            // Sensor acquisition
            sensorAcquisition(); // call the routine

            //Get the phase of Smart Grid
            phaser_SG();
            //update_PLL_SG();

            DQcontrolSG(); // call the routine


            //State evolution
            compt--;
            if(compt<=0) // start-up done ? (compt=40000 -> 2s)
            {
                //iRefd = 1;//TODO DEBUG
                GPIO_writePin(DEVICE_GPIO_PIN_LED1,0);// switch on the LED
                MainState=PARKCONTROL;
            }

            break;
        }

    /*##########################
    ### Smart Grid operation
    ############################*/
    case PARKCONTROL : // Asservissement triphase avec la methode de PARK + absorption sinus
        {

            //PWM watchdog service
            GPIO_togglePin(GPIO_WC);

            /*##########################
            ### Sensor acquisition
            ############################*/

            sensorAcquisition(); // call the routine


            /*##########################
            ### DQ control of Smart Grid
            ############################*/

            //### update phase ###

            /*//This is in the case of voltage synthesis only. Otherwise, a PLL or phaser should be used
            //
            //interrupt every 50�s i.e. 400 interrupt in 20ms (1/50Hz)
            //phase is from 0 to 65535, 65535/400~164
            phaseSG += refFreqSG;*/

            //Get the phase of Smart Grid
            phaser_SG();
            //update_PLL_SG();

            DQcontrolSG(); // call the routine that does:
            // 1- compute park transform of the currents
            // 2- update controller for close loop on id and iq
            // 3- call reverse Park and update PWM for Smart Grid inverter



            /*##########################
            ### Sine absorption rectifier control
            ############################*/

            /*PFCcontrolEDF();// call the routine that does:
            // 1- update DC bus voltage controller
            // 2- compute a Phase Lock Loop on EDF voltage
            // 3- generate input current reference
            // 4- update controller for close loop curent control
            // 5- update PWM for EDF inverter
            */

            break;
        }

    /*##########################
    ### Idle state
    ############################*/
    case IDLE_STATE :
        {
            sensorAcquisition(); // call the routine
            GPIO_writePin(DEVICE_GPIO_PIN_LED1,1);// switch off the LED

            //and just chill!
            break;
        }

    /*##########################
    ### Open loop control
    ############################*/
    case OPENLOOP :
        {

            //PWM watchdog service
            GPIO_togglePin(GPIO_WC);

            /*##########################
            ### Sensor acquisition
            ############################*/
            sensorAcquisition(); // call the routine

            /*##########################
            ### Phase update
            ############################*/
            //This is in the case of voltage synthesis only.
            //interrupt every 50µs i.e. 400 interrupt in 20ms (1/50Hz)
            //phase is from 0 to 65535, 65535/400~164
            phaseSG += refFreqSG;

            /*##########################
            ### Open loop PWM control
            ############################*/
            //parkinverse(0.6, 0, 0, phaseSG);// call reverse Park and update PWM -- FULL AMPLITUDE
            parkinverse(0.4, 0, 0, phaseSG);// call reverse Park and update PWM


            /*##########################
            ### Update dq0 vars for monitoring
            ############################*/
            i0 = 0.333333*(ia+ib+ic);
            id = ROOT2OVER3*(ia*sinTable[(phaseSG+16384)>>8]+ib*sinTable[(phaseSG-5461)>>8]+ic*sinTable[(phaseSG+(uint16_t)38229)>>8]);
            iq = ROOT2OVER3*(-ia*sinTable[phaseSG>>8]-ib*sinTable[(phaseSG-21845)>>8]-ic*sinTable[(phaseSG+21845)>>8]);

            v0 = 0.333333*(vAN+vBN+vCN);// should always be zero (by construction of vXN)
            vd = ROOT2OVER3*(vAN*sinTable[(phaseSG+16384)>>8]+vBN*sinTable[(phaseSG-5461)>>8]+vCN*sinTable[(phaseSG+(uint16_t)38229)>>8]);
            vq = ROOT2OVER3*(-vAN*sinTable[phaseSG>>8]-vBN*sinTable[(phaseSG-21845)>>8]-vCN*sinTable[(phaseSG+21845)>>8]);

            break;
        }

    default : // switch to Idle
        {
            MainState=IDLE_STATE;
        }

    }

    /*######################
    ### Statistics update
    ########################*/

    updateStatistics();


    // For the interrupt
    EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}



// #######################################################################
// External TIMER1 Interrupt
//
interrupt void LowSpeed_interrupt(void)
{
    SendSerial = true; // It is now time to send info2serial
    // but this is not done in this interrupt, because it is time consuming and must be interruptible.
}









//
// End of File
//
