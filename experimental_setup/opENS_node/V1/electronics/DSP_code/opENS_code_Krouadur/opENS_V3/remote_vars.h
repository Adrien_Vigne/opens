/*
 * remote_vars.h
 *
 *  Created on: 21 août 2020
 *      Author: gjodi141
 *
 *      This file declare the variables that can be accessed through serial connection.
 *      The .C file associated with this one only should be considered while setting another program that while interact with the DSP (like a Python script on the Raspberry Pi...)
 *      Functions Get and Set are also declared.
 */

#ifndef REMOTE_VARS_H_
#define REMOTE_VARS_H_

#include "myheader.h"

//
// Variables
//
extern const void *VarAddr[];// table containing all the share variable addresses


//
// Function Prototypes
//
bool ManageRemoteVarsSerial(uint16_t);// check the received char and manage the Get and Set command, return true if so


#endif /* REMOTE_VARS_H_ */
