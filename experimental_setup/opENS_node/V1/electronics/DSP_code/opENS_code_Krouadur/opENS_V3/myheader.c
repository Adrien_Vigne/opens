/*
 * myheader.c
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      Here are the global definition of variables that cannot be defined constants.
 *      Initialization functions are here.
 *      Plus a few functions that have no other file to be in.
 */

#include "myheader.h"

//
// Variable (or global constant definitions)
//

// Sinus table
const float sinTable[256]={0.00000000, 0.02454123, 0.04906767, 0.07356456, 0.09801714, 0.12241068, 0.14673047, 0.17096189, 0.19509032, 0.21910124, 0.24298018, 0.26671276, 0.29028468, 0.31368174, 0.33688985, 0.35989504, 0.38268343, 0.40524131, 0.42755509, 0.44961133, 0.47139674, 0.49289819, 0.51410274, 0.53499762, 0.55557023, 0.57580819, 0.59569930, 0.61523159, 0.63439328, 0.65317284, 0.67155895, 0.68954054, 0.70710678, 0.72424708, 0.74095113, 0.75720885, 0.77301045, 0.78834643, 0.80320753, 0.81758481, 0.83146961, 0.84485357, 0.85772861, 0.87008699, 0.88192126, 0.89322430, 0.90398929, 0.91420976, 0.92387953, 0.93299280, 0.94154407, 0.94952818, 0.95694034, 0.96377607, 0.97003125, 0.97570213, 0.98078528, 0.98527764, 0.98917651, 0.99247953, 0.99518473, 0.99729046, 0.99879546, 0.99969882, 1.00000000, 0.99969882, 0.99879546, 0.99729046, 0.99518473, 0.99247953, 0.98917651, 0.98527764, 0.98078528, 0.97570213, 0.97003125, 0.96377607, 0.95694034, 0.94952818, 0.94154407, 0.93299280, 0.92387953, 0.91420976, 0.90398929, 0.89322430, 0.88192126, 0.87008699, 0.85772861, 0.84485357, 0.83146961, 0.81758481, 0.80320753, 0.78834643, 0.77301045, 0.75720885, 0.74095113, 0.72424708, 0.70710678, 0.68954054, 0.67155895, 0.65317284, 0.63439328, 0.61523159, 0.59569930, 0.57580819, 0.55557023, 0.53499762, 0.51410274, 0.49289819, 0.47139674, 0.44961133, 0.42755509, 0.40524131, 0.38268343, 0.35989504, 0.33688985, 0.31368174, 0.29028468, 0.26671276, 0.24298018, 0.21910124, 0.19509032, 0.17096189, 0.14673047, 0.12241068, 0.09801714, 0.07356456, 0.04906767, 0.02454123, 0.00000000, -0.02454123, -0.04906767, -0.07356456, -0.09801714, -0.12241068, -0.14673047, -0.17096189, -0.19509032, -0.21910124, -0.24298018, -0.26671276, -0.29028468, -0.31368174, -0.33688985, -0.35989504, -0.38268343, -0.40524131, -0.42755509, -0.44961133, -0.47139674, -0.49289819, -0.51410274, -0.53499762, -0.55557023, -0.57580819, -0.59569930, -0.61523159, -0.63439328, -0.65317284, -0.67155895, -0.68954054, -0.70710678, -0.72424708, -0.74095113, -0.75720885, -0.77301045, -0.78834643, -0.80320753, -0.81758481, -0.83146961, -0.84485357, -0.85772861, -0.87008699, -0.88192126, -0.89322430, -0.90398929, -0.91420976, -0.92387953, -0.93299280, -0.94154407, -0.94952818, -0.95694034, -0.96377607, -0.97003125, -0.97570213, -0.98078528, -0.98527764, -0.98917651, -0.99247953, -0.99518473, -0.99729046, -0.99879546, -0.99969882, -1.00000000, -0.99969882, -0.99879546, -0.99729046, -0.99518473, -0.99247953, -0.98917651, -0.98527764, -0.98078528, -0.97570213, -0.97003125, -0.96377607, -0.95694034, -0.94952818, -0.94154407, -0.93299280, -0.92387953, -0.91420976, -0.90398929, -0.89322430, -0.88192126, -0.87008699, -0.85772861, -0.84485357, -0.83146961, -0.81758481, -0.80320753, -0.78834643, -0.77301045, -0.75720885, -0.74095113, -0.72424708, -0.70710678, -0.68954054, -0.67155895, -0.65317284, -0.63439328, -0.61523159, -0.59569930, -0.57580819, -0.55557023, -0.53499762, -0.51410274, -0.49289819, -0.47139674, -0.44961133, -0.42755509, -0.40524131, -0.38268343, -0.35989504, -0.33688985, -0.31368174, -0.29028468, -0.26671276, -0.24298018, -0.21910124, -0.19509032, -0.17096189, -0.14673047, -0.12241068, -0.09801714, -0.07356456, -0.04906767, -0.02454123};

extern uint16_t currentValue = 0; // debug
unsigned char* tmpString; // contains manipulated string to be output to USB serial


// #######################################################################
// configCPUTimer - This function initializes the selected timer to the
// period specified by the "freq" and "period" parameters. The "freq" is
// entered as Hz and the period in uSeconds. The timer is held in the stopped
// state after configuration.
//
void configCPUTimer(uint32_t cpuTimer, float freq, float period)
{
    uint32_t temp;

    //
    // Initialize timer period:
    //
    temp = (uint32_t)(freq / 2000000 * period);
    CPUTimer_setPeriod(cpuTimer, temp);

    //
    // Set pre-scale counter to divide by 1 (SYSCLKOUT):
    //
    CPUTimer_setPreScaler(cpuTimer, 0);

    //
    // Initializes timer control register. The timer is stopped, reloaded,
    // free run disabled, and interrupt enabled.
    // Additionally, the free and soft bits are set
    //
    CPUTimer_stopTimer(cpuTimer);
    CPUTimer_reloadTimerCounter(cpuTimer);
    CPUTimer_setEmulationMode(cpuTimer,CPUTIMER_EMULATIONMODE_STOPAFTERNEXTDECREMENT);
    CPUTimer_enableInterrupt(cpuTimer);

}





void myInitialization(void)
{
    // ###############################################################
    // ### Initialization

    // Initialize device clock and peripherals
    Device_init(); // initial configuration

    // ### I/O pin settings
    //
    // Initialize GPIO and configure the GPIO pin as a push-pull output
    Device_initGPIO();
    GPIO_setPadConfig(DEVICE_GPIO_PIN_LED1, GPIO_PIN_TYPE_STD); // to blink the LED
    GPIO_setDirectionMode(DEVICE_GPIO_PIN_LED1, GPIO_DIR_MODE_OUT);
    GPIO_writePin(DEVICE_GPIO_PIN_LED1,0);// switch off the LED

    GPIO_setPadConfig(GPIO_WC, GPIO_PIN_TYPE_STD); // Watchdog
    GPIO_setDirectionMode(GPIO_WC, GPIO_DIR_MODE_OUT);

    GPIO_setPadConfig(GPIO_DEBUG, GPIO_PIN_TYPE_STD); // Debug
    GPIO_setDirectionMode(GPIO_DEBUG, GPIO_DIR_MODE_OUT);

    //### Interrupt ###
    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Assign the interrupt service routines to ePWM interrupts
    //
    Interrupt_register(INT_EPWM1, &HighSpeed_interrupt);
    //
    // ISRs for each CPU Timer interrupt
    //
    Interrupt_register(INT_TIMER1, &LowSpeed_interrupt);
    //
    // Configure CPU-Timer 1 to interrupt
    // Period (in uSeconds)
    // void configCPUTimer(uint32_t cpuTimer, float freq, float period)
    //
    configCPUTimer(CPUTIMER1_BASE, DEVICE_SYSCLK_FREQ, 500); // period in �s = 1000->1ms
    //
    // To ensure precise timing, use write-only instructions to write to the
    // entire register. Therefore, if any of the configuration bits are changed
    // in configCPUTimer and initCPUTimers, the below settings must also
    // be updated.
    //
    CPUTimer_enableInterrupt(CPUTIMER1_BASE);

    // ### PWM setup
    initPWMs(); // initialize the PWM 1,2 and 3 with the corresponding pins
    startPWM();


    //
    // Enable ePWM interrupts
    //
    Interrupt_enable(INT_EPWM1);
    //
    // Enables CPU int1, int13, and int14 which are connected to CPU-Timer 1.
    // Enable TINT0 in the PIE: Group 1 interrupt 7
    //
    Interrupt_enable(INT_TIMER1);

    //
    // Starts CPU-Timer 0, CPU-Timer 1, and CPU-Timer 2.
    //
    /* Low speed log only
    CPUTimer_startTimer(CPUTIMER1_BASE);*/
    //
    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    //
    EINT;
    ERTM;

    // ### ADC setup
    initADCs();// power up the hardware
    initADCSOCs();// set software trigger

    // ### USB serial connection settings
    initUSBserial(); // initial USB serial

    //initilize all PWM at 50%
    updatePWM(PWMd, .5);// sortie puissance #2 V
    updatePWM(PWMe, .5);// sortie puissance #2 U
    updatePWM(PWMa, .5);// sortie puissance C
    updatePWM(PWMb, .5);// sortie puissance B
    updatePWM(PWMc, .5);// sortie puissance A
}
