/*
 * myDebugTools.c
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *
 *      Here are the tools I use to debug and send data though serial connection.
 *      The serial interface through keyboard is also managed here.
 *
 *  2020-08-25 High speed logs configurable with share vars
 */

#include "myDebugTools.h"

//
// Variable initialization
//

// counter for data to be logged
uint16_t LOGcount = LOGpoints+1;

// Serial connection (low speed log enabling)
bool SendSerial=false;

float mem1[LOGpoints];
float mem2[LOGpoints];
float mem3[LOGpoints];

// High speed logs setting
extern uint16_t i_mem1 = 4;//indices of ?
extern uint16_t i_mem2 = 5;//indices of
extern uint16_t i_mem3 = 6;//indices of



/* ######################################################
 * #  info2serial
 * # Used to send some results to serial, at low speed.
 * #
 * ################################
 */
void info2serial(void)
    {
    //TODO
    float tmpF[13];


    tmpF[0] = iRefd;
    tmpF[1] = id;
    tmpF[2] = iRefq;
    tmpF[3] = iq;
    tmpF[4] = Iaeff;
    tmpF[5] = gainVA;

    /*tmpF[0] = Vamoy;
    tmpF[1] = Vaeff;
    tmpF[2] = vNGmoy;
    tmpF[3] = Iamoy;
    tmpF[4] = Iaeff;
    tmpF[5] = Vudcmoy;
    tmpF[6] = ia2;
    tmpF[7] = gainVA;
    tmpF[8] = gainVB;
    tmpF[9] = gainVC;
    tmpF[10] = gainIA;
    tmpF[11] = gainIB;
    tmpF[12] = gainIC;*/


    uint16_t i;
    for (i=0; i<=0; ++i)
       {
           ftoa(tmpF[i], tmpString, 5);
           USBserialSend(tmpString);
           USBserialSend(" \0");
       }

    for (i=1; i<=4; ++i)
       {
           ftoa(tmpF[i], tmpString, 5);
           USBserialSend(tmpString);
           USBserialSend(" \0");
       }

    ftoa(tmpF[5], tmpString, 5);
    USBserialSend(tmpString);
    USBserialSend("\r\n\0");


    /*// All voltages and currents
    tmpF[0] = Iamoy;
    tmpF[1] = Iaeff;
    tmpF[2] = Ibmoy;
    tmpF[3] = Ibeff;
    tmpF[4] = Icmoy;
    tmpF[5] = Iceff;
    tmpF[6] = Iresmoy;
    tmpF[7] = Ireseff;
    tmpF[8] = Vresmoy;
    tmpF[9] = Vreseff;
    tmpF[10] = Vamoy;
    tmpF[11] = Vaeff;
    tmpF[12] = Vbmoy;
    tmpF[13] = Vbeff;
    tmpF[14] = Vcmoy;
    tmpF[15] = Vceff;
    tmpF[16] = vNGmoy;
    tmpF[17] = vNGeff;
    tmpF[18] = Vudcmoy;
     */

}




/* ######################################################
 * #  LowPriorityLoop_logs
 * # Manage the logs through serial connection.
 * # Both Hi-speed and hi-speed logs are managed.
 * #
 * # /!\This slow routine should be called from an interruptible function.
 * ################################
 */
void LowPriorityLoop_logs(void)
{
    // Low speed log
    if (SendSerial) // info log: Is it time to send information through serial connection?
            {
                info2serial();
                SendSerial = false;
            }
    //High speed log
    if (LOGcount==LOGpoints)// time to display the info
    {
        LOGcount++;// won't happen another time
        uint16_t i;
        for (i=0; i<LOGpoints; ++i)// loop to send all data to serial
        {
            ftoa(mem1[i], tmpString, 3);
            USBserialSend(tmpString);
            USBserialSend(" \0");
            ftoa(mem2[i], tmpString, 3);
            USBserialSend(tmpString);
            USBserialSend(" \0");
            ftoa(mem3[i], tmpString, 3);
            USBserialSend(tmpString);
            USBserialSend("\r\n\0");
        }
    }
}

/* ######################################################
 * #  HighSpeedLogs
 * # Manage the high speed log. If logs are enabled, the data
 * # are stored in the memX variables.
 * # The variables are selected by setting i_mem1, i_mem2 and i_mem3
 * # indices.
 * #
 * # /!\This slow routine should be called every 50�s.
 * ################################
 */
void HighSpeedLogs(void)
{
    // Use this time to store previous values to LOG
    if (LOGcount<LOGpoints)
    {

        mem1[LOGcount] = (*(float *)VarAddr[i_mem1]);
        mem2[LOGcount] = (*(float *)VarAddr[i_mem2]);
        mem3[LOGcount] = (*(float *)VarAddr[i_mem3]);

        LOGcount++;
    }
}

/* ######################################################
 * #  LowPriorityLoop_KeyboardInterface(uint16_t receivedChar)
 * # Human interface through keyboard and serial-connection.
 * # receivedChar : the received char from keyboard.
 * #
 * # /!\This slow routine should be called from an interruptible function.
 * ################################
 */
void LowPriorityLoop_KeyboardInterface(uint16_t receivedChar)
{
    switch (receivedChar)
    {
    // PI dq
    case 'p' : // p = increase P
        kIdq += kPdq;
        kPdq *= 1.05;//+5%
        kIdq -= kPdq;// to keep the same integral parameter
        break;
    case 'm' : // m = decrease P
        kIdq += kPdq;
        kPdq *= 0.9524;//=1/1.05
        kIdq -= kPdq;// to keep the same integral parameter
        break;
    case 'i' : // i = increase I
        kIdq = 1.05 * kIdq + 0.05 * kPdq;//+5% compensate kPdq effect
        break;
    case 'k' : // k = decrease I
        kIdq = 0.9524 * kIdq -0.0476 * kPdq;// compensate kPdq effect
        break;
    // PI 0
    case 'u' : // p = increase P
        kI0 += kP0;
        kP0 *= 1.05;//+5%
        kI0 -= kP0;// to keep the same integral parameter
        break;
    case 'j' : // m = decrease P
        kI0 += kP0;
        kP0 *= 0.9524;//=1/1.05
        kI0 -= kP0;// to keep the same integral parameter
        break;
    case 'y' : // i = increase I
        kI0 = 1.05 * kI0 + 0.05 * kP0;//+5% compensate kP0 effect
        break;
    case 'h' : // k = decrease I
        kI0 = 0.9524 * kI0 -0.0476 * kP0;// compensate kP0 effect
        break;

    case ' ' : // start/stop info log
        //High speed log
        LOGcount = 0;// start from 0 the log
        break;
    case 'v' : // start/stop info log
        //Here is for low speed log.
        // stop or resume the timer
        HWREGH(CPUTIMER1_BASE + CPUTIMER_O_TCR) ^= CPUTIMER_TCR_TSS;// Toggle TSS bit of register TCR of CPUTIMER1_BASE
        break;
    case 'a' : // a = increase iRefd
        iRefd += 0.1;
        break;
    case 'q' : // a = decrease iRefd
        iRefd -= 0.1;
        break;
    case 'z' : // a = increase iRefq
        iRefq += 0.1;
        break;
    case 's' : // a = decrease iRefq
        iRefq -= 0.1;
        break;
    case 'r' : // r = reset sensors
        MainState = CALIB_SENSOR;
        break;
    case 't' : // t = idle state
        MainState = IDLE_STATE;
        USBserialSend("Idle\r\n\0");
        break;
    case 'g' : // g = Go to PFC then PARK
        compt = TIME_STARTPFC;// reset counter
        MainState=STARTPFC;
        break;

    case '#' :
        USBserialSend("Open loop power!\r\n\0");
        MainState=OPENLOOP;
        break;

    default :
        USBserialSend("kP = \0");
        ftoa(kPdq, tmpString, 6);
        USBserialSend(tmpString);
        USBserialSend("\tkI = \0");
        ftoa(kIdq, tmpString, 6);
        USBserialSend(tmpString);

        USBserialSend("kP0 = \0");
        ftoa(kP0, tmpString, 6);
        USBserialSend(tmpString);
        USBserialSend("\tkI0 = \0");
        ftoa(kI0, tmpString, 6);
        USBserialSend(tmpString);

        USBserialSend("\tid = \0");
        ftoa(iRefd, tmpString, 6);
        USBserialSend(tmpString);
        USBserialSend("\tiq = \0");
        ftoa(iRefq, tmpString, 6);
        USBserialSend(tmpString);
        USBserialSend("\r\n\0");
        break;
    }


}

