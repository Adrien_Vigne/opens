/*
 * Gus 04-03-2019
 * from adc_soc_software_sync_cpu01.c
 *
 * ADC Synchronous SOC Software Force (adc_soc_software_sync)
 *
 *
 *
 *  2020-01-20 Adding adcB4
 *
 *  2020-01-31 Some info on ADC:
 *      conversion between VREFLO and VREFHI. These refs can be different for each
 *      port. On the LaunchPad XL, all VREFLO = 0V and VREFHIA=VREFHIC, and VREFHIB =
 *      VREFHID. The two diffirent IC to get VREFHIx are two rail to rail amplifier
 *      that follow the voltage reference from REF5030IDGKT -> stable low noise 3V
 *      reference. So, on 12bits ADC => 0V -> 0 and 3V -> 4096
 *
 */

#include "myADC.h"

// Global variables
uint16_t adcA0;
uint16_t adcA2;
uint16_t adcB2;
uint16_t adcC2;
uint16_t adcA3;
uint16_t adcB3;
uint16_t adcC3;
uint16_t adcA1;
uint16_t adcA4;
uint16_t adcB4;

// Function to configure and power up ADCs A, B and C.
void initADCs(void)
{
    //
    // Set ADCCLK divider to /4
    //
    ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_1_0);// ADC_CLK_DIV_4_0
    ADC_setPrescaler(ADCB_BASE, ADC_CLK_DIV_1_0);
    ADC_setPrescaler(ADCC_BASE, ADC_CLK_DIV_1_0);

    //
    // Set resolution and signal mode (see #defines above) and load
    // corresponding trims.
    //
#if(EX_ADC_RESOLUTION == 12)
    ADC_setMode(ADCA_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);
    ADC_setMode(ADCB_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);
    ADC_setMode(ADCC_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);
#elif(EX_ADC_RESOLUTION == 16)
    ADC_setMode(ADCA_BASE, ADC_RESOLUTION_16BIT, ADC_MODE_DIFFERENTIAL);
    ADC_setMode(ADCB_BASE, ADC_RESOLUTION_16BIT, ADC_MODE_DIFFERENTIAL);
    ADC_setMode(ADCC_BASE, ADC_RESOLUTION_16BIT, ADC_MODE_DIFFERENTIAL);
#endif

    //
    // Set pulse positions to late
    //
    ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);
    ADC_setInterruptPulseMode(ADCB_BASE, ADC_PULSE_END_OF_CONV);
    ADC_setInterruptPulseMode(ADCC_BASE, ADC_PULSE_END_OF_CONV);

    //
    // Power up the ADCs and then delay for 1 ms
    //
    ADC_enableConverter(ADCA_BASE);
    ADC_enableConverter(ADCB_BASE);
    ADC_enableConverter(ADCC_BASE);

    DEVICE_DELAY_US(1000);
}


// Function to configure SOCs 0 and 1 of ADCs A and B.
//
void initADCSOCs(void)
{
    // Some help: here is for all ADC from A port. If we want to use
    // ADCINA5, we set here (A) one available SOCx on ADC_CH_ADCIN5.
    // ########################################################
    // Configure SOCs of ADCA
    // - SOC0 will convert pin A0.
    // - SOC1 will convert pin A2.
    // - SOC2 will convert pin A3.
    // - Both will be triggered by software only.
    // - For 12-bit resolution, a sampling window of 15 (75 ns at a 200MHz
    //   SYSCLK rate) will be used.  For 16-bit resolution, a sampling window
    //   of 64 (320 ns at a 200MHz SYSCLK rate) will be used.
    //
#if(EX_ADC_RESOLUTION == 12)
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN0, 30);// 15 avant
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 30);
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER2, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN3, 30);
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER3, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN1, 30);
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER4, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN4, 30);
#elif(EX_ADC_RESOLUTION == 16)
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN0, 64);
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 64);
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER2, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN3, 64);
#endif

    //
    // Set SOC1 to set the interrupt 1 flag. Enable the interrupt and make
    // sure its flag is cleared.
    //
    ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER4); // ADC_SOC_NUMBER4        #################
    ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);


    // ########################################################
    // Configure SOCs of ADCB
    // - SOC0 will convert pin B2.
    // - SOC1 will convert pin B3.
    // - SOC2 will convert pin B4
    // - All pins will be triggered by software only.
    // - For 12-bit resolution, a sampling window of 15 (75 ns at a 200MHz
    //   SYSCLK rate) will be used.  For 16-bit resolution, a sampling window
    //   of 64 (320 ns at a 200MHz SYSCLK rate) will be used.
    //
#if(EX_ADC_RESOLUTION == 12)
    ADC_setupSOC(ADCB_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 30);
    ADC_setupSOC(ADCB_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN3, 30);
    ADC_setupSOC(ADCB_BASE, ADC_SOC_NUMBER2, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN4, 30);
#elif(EX_ADC_RESOLUTION == 16)
    ADC_setupSOC(ADCB_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 64);
    ADC_setupSOC(ADCB_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN3, 64);
#endif

    //
    // Set SOC1 to set the interrupt 1 flag. Enable the interrupt and make
    // sure its flag is cleared.
    //
    ADC_setInterruptSource(ADCB_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER1);
    ADC_enableInterrupt(ADCB_BASE, ADC_INT_NUMBER1);
    ADC_clearInterruptStatus(ADCB_BASE, ADC_INT_NUMBER1);


    // ########################################################
    // Configure SOCs of ADCC
    // - SOC0 will convert pin C2.
    // - SOC1 will convert pin C3.
    // - Both will be triggered by software only.
    // - For 12-bit resolution, a sampling window of 15 (75 ns at a 200MHz
    //   SYSCLK rate) will be used.  For 16-bit resolution, a sampling window
    //   of 64 (320 ns at a 200MHz SYSCLK rate) will be used.
    //
#if(EX_ADC_RESOLUTION == 12)
    ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 30);
    ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN3, 30);
#elif(EX_ADC_RESOLUTION == 16)
    ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 64);
    ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN3, 64);
#endif

    //
    // Set SOC1 to set the interrupt 1 flag. Enable the interrupt and make
    // sure its flag is cleared.
    //
    ADC_setInterruptSource(ADCC_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER1);
    ADC_enableInterrupt(ADCC_BASE, ADC_INT_NUMBER1);
    ADC_clearInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1);
}



// start a conversion
//
void startConvADC(void)
{
    // Write the registers to start the sampling  on the different SOCs
    //
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER0);
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER1);
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER2);
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER3); //##########
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER4); //##########
    ADC_forceSOC(ADCB_BASE, ADC_SOC_NUMBER0);
    ADC_forceSOC(ADCB_BASE, ADC_SOC_NUMBER1);
    ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER0);
    ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER1);
    ADC_forceSOC(ADCB_BASE, ADC_SOC_NUMBER2);

}



// send true once the results are available and stored in the adcXi variables
//
bool ADCresultsReady(void)
{
    if ((ADC_getInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1) == true) // ADC A conversion done
            && (ADC_getInterruptStatus(ADCB_BASE, ADC_INT_NUMBER1) == true) // and ADC B conversion done
            && (ADC_getInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1) == true)) // and ADC C conversion done
    {
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1); // clear flag ADC A
        ADC_clearInterruptStatus(ADCB_BASE, ADC_INT_NUMBER1); // clear flag ADC B
        ADC_clearInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1); // clear flag ADC B

        // store the results in the global variables adcXi
        adcA0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        adcA2 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
        adcA3 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER2);
        adcA1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER3); //##########
        adcA4 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER4); //##########
        adcB2 = ADC_readResult(ADCBRESULT_BASE, ADC_SOC_NUMBER0);
        adcB3 = ADC_readResult(ADCBRESULT_BASE, ADC_SOC_NUMBER1);
        adcC2 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
        adcC3 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER1);
        adcB4 = ADC_readResult(ADCBRESULT_BASE, ADC_SOC_NUMBER2);

        return true;// let's go
    }
    else
        return false;// not ready yet
}
