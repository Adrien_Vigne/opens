/*
 * definitions.h
 *
 *  Created on: 22 janv. 2020
 *      Author: Gurvan
 *
 *      for V3.0.1
 */

#ifndef MYHEADER_H_
#define MYHEADER_H_

//
// Program version
//
#define VERSION "3.3"


//
// opENS node configuration choice
//  #       1       2       3       4       5   // A3/A4/A5 ajouté par Rachel Neveu 24/07/2020
// config.  A1      A2      A3      A4      A5
#define OPENS_CONF 3


//
// Included Files
//
#include "driverlib.h"
#include "device.h"

#include "myADC.h" // to manage the Analog->Digital converters in a synchronous way
#include "myPWM.h" // to manage the PWM
#include "myUSBserial.h" // to manage the serial connection through USB
#include <string.h>
#include "math.h"

#include "myDebugTools.h" // used for debug and human interface, using serial connections
#include "parkcontrol.h" // used for park calculation and control
#include "acquisition.h" // used for sampling and computing statistics of analog inputs
#include "statemachine.h" // used for managing the control states of the DSP

#include "remote_vars.h" // declare variables that can be get and set from serial connection

//
// Routine declaration
//
// Functions here do not belong somewhere else. It is default location.

void configCPUTimer(uint32_t, float, float); // setup a timer to a given frequency
void myInitialization(void); // Initialization functions

//Interrupt functions
extern interrupt void HighSpeed_interrupt(void); // every 50�s (PWM freq, for controllers)
extern interrupt void LowSpeed_interrupt(void); // around 500�s to 1ms (mainly serial debuger)

//
// Definitions
//

// I/O pins
#define GPIO_WC     61U // Watchdog pin
#define GPIO_DEBUG     123U//for oscillo debug, just under P61

// Constants
#define VALBITMAX 65535 // 2^N-1
#define DEUXPI 6.28318530718
#define ROOT2OVER3 0.8165
#define Ts 50e-6

#define NOMINAL_U_DC 340 //Nominal DC bus voltage for regulation, 340V is standard, less is for development

// Sinus table
extern const float sinTable[256];


// Serial connection variables
extern unsigned char* tmpString; // contains manipulated string to be output to USB serial
extern uint16_t currentValue;




//
// opENS node configuration
//
/*#ifndef OPENS_CONF
#warning "OPENS_CONF is not defined!"
#endif*/

#if OPENS_CONF == 1
 #warning "OPENS_CONF -> A1"
 #include "conf_A1.h"
#elif OPENS_CONF == 2
 #warning "OPENS_CONF -> A2"
 #include "conf_A2.h"
#elif OPENS_CONF == 3
 #warning "OPENS_CONF -> A3"
 #include "conf_A3.h"
#elif OPENS_CONF == 4
 #warning "OPENS_CONF -> A4"
 #include "conf_A4.h"
#elif OPENS_CONF == 5
 #warning "OPENS_CONF -> A5"
 #include "conf_A5.h"
#else
 #error "OPENS_CONF is not defined!"
#endif



#endif /* MYHEADER_H_ */
