/*
 * remote_vars.c
 *
 *  Created on: 21 août 2020
 *      Author: gjodi141
 *      This file declare the variables that can be accessed through serial connection.
 *      This only file should be considered while setting another program that while interact with the DSP (like a Python script on the Raspberry Pi...)
 *      Then implementation of functions to SET and GET data are implemented.
 */


#include "remote_vars.h"

/*
 * 2020-08-21 version 0 >> first development version
 *
 */

#define remoteVarVersion "5"
#define remoteVar_numberOfVars 41
#define remoteVar_numberOfUINT16_Tvars 5 //number of integer vars before the floats
const void *VarAddr[remoteVar_numberOfVars] = {
                           &MainState, //0 <-- MainState is an uint16_t, special care is needed
                           &i_mem1,
                           &i_mem2,
                           &i_mem3,
                           &configCalib,
                           &iRefq,  //5    // all the other variables are float
                           &iRefd,
                           &id,
                           &iq,
                           &i0,
                           &ia,//10
                           &Iamoy,
                           &Iaeff,
                           &ib,
                           &Ibmoy,
                           &Ibeff,//15
                           &ic,
                           &Icmoy,
                           &Iceff,
                           &vaG,
                           &Vamoy,//20
                           &Vaeff,
                           &vbG,
                           &Vbmoy,
                           &Vbeff,
                           &vcG,//25
                           &Vcmoy,
                           &Vceff,
                           &Vudcmoy,
                           &vd,
                           &vq,//30
                           &v0,
                           &kPdq,
                           &kIdq,
                           &ia_ref,
                           &ib_ref,//35
                           &ic_ref,
                           &sinSyncSG,
                           &DEBUGprobeVar1,
                           &DEBUGprobeVar2,
                           &DEBUGprobeVar3//40
                           };







/* ######################################################
 * #  ManageRemoteVarsSerial(uint16_t firstReceivedChar)
 * # Manage the remote variables through serial connection.
 * # firstReceivedChar contains a char from serial. If it is 'G', 'S' or 'V', it
 * # will continue the readings to deal with Get and Set functions.
 * # Return TRUE if it was Set or Get.
 * #
 * # /!\This routine should be called from an interruptible function.
 * ################################
 */
bool ManageRemoteVarsSerial(uint16_t firstReceivedChar)
{
    int16_t i;
    float f;


    // ####
    // ## Get function
    if (firstReceivedChar=='G')
    {
        //ex. "G 12\0"
        //receive up to 4 chars or end with \0 char (first chars may be spaces, it works)
        getString(4, 0, tmpString);

        //convert the string to var number
        i = atoi(tmpString);

        // check variable range
        if (i<0 || i>remoteVar_numberOfVars-1) {
            memcpy(tmpString,"NaN\0",5);
        }
        else { // process request
            if (i<remoteVar_numberOfUINT16_Tvars) // specific case of uint16_t data
                itoa((*(uint16_t *)VarAddr[i]), tmpString, 10);
            else // float
                ftoa((*(float *)VarAddr[i]), tmpString, 3);
        }

        // answer data
        USBserialSend(tmpString);

        return true;// we did deal with a RemoteVar command
    }

    if (firstReceivedChar=='S')
    {
        // Set function
        //ex. S 12 3.14\0"
        //receive up to 4 chars or end with space (first chars may be spaces, it works)
        getString(4, ' ', tmpString);

        //convert the string to var number
        i = atoi(tmpString);

        //receive up to 7 chars or end with \0 char (first chars may be spaces, it works)
        getString(7, 0, tmpString);

        //convert the string to float number
        f = atof(tmpString);

        // check variable range
        if (i<0 || i>remoteVar_numberOfVars-1) {
            return true; // just leave
        }
        else { // process request
            if (i<remoteVar_numberOfUINT16_Tvars) // specific case of uint16_t data
                *(uint16_t *)VarAddr[i] = (uint16_t)f;
            else // float
                *(float *)VarAddr[i] = f;
        }


        return true;// we did deal with a RemoteVar command
    }

    // ####
    // ## Version function
    if (firstReceivedChar=='V')
    {
        //Directly send back "v"+VERSION+" "remoteVarVersion + \r
        USBserialSend("v" VERSION " " remoteVarVersion "\r");

        return true;// we did deal with a RemoteVar command
    }

    if (firstReceivedChar=='R')
    {
        // Reset function

        SysCtl_setWatchdogMode(SYSCTL_WD_MODE_RESET);
        SysCtl_enableWatchdog();

        return true;// we did deal with a RemoteVar command
    }


    return false; // it is something else, let other routine deal with it



}

