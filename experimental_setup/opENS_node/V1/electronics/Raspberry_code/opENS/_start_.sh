#!/bin/bash

#              ______ _   _  _____
#             |  ____| \ | |/ ____|
#   ___  _ __ | |__  |  \| | (___
#  / _ \| '_ \|  __| | . ` |\___ \
# | (_) | |_) | |____| |\  |____) |
#  \___/| .__/|______|_| \_|_____/
#       | |
#       |_|
#
# G. Jodin 2020-09-25
#

#This script is run when the Pi finished booting on the OS.
echo 'All logs in /home/pi/opENS/log.txt'


cat /home/pi/opENS/opENS_ASCII_logo >> /home/pi/opENS/log.txt

date >> /home/pi/opENS/log.txt  #display today's date

#start tactile screen
#sudo service lightdm stop
sudo service lightdm start
export DISPLAY=:0

# execute main script
#python3 /home/pi/Documents/ExchangePiPC/Pi_app0/app1.py >> /home/pi/opENS/log.txt
