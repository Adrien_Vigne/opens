#!/bin/bash

#              ______ _   _  _____
#             |  ____| \ | |/ ____|
#   ___  _ __ | |__  |  \| | (___
#  / _ \| '_ \|  __| | . ` |\___ \
# | (_) | |_) | |____| |\  |____) |
#  \___/| .__/|______|_| \_|_____/
#       | |
#       |_|
#
# G. Jodin 2020-10-06
#

#This script is run when the Pi finished booting on the OS.
cat /home/pi/opENS/opENS_ASCII_logo

date >> /home/pi/opENS/log.txt  #display today's date

#start tactile screen
#sudo service lightdm stop
sudo service lightdm start
export DISPLAY=:0

# execute main script
python3 /home/pi/opENS/opENSs/opENSs1.py
