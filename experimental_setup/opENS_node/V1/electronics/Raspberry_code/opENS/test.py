#!/usr/bin/env python

#test to display something


import matplotlib
matplotlib.use('TkAgg')
# matplotlib.rcParams['toolbar'] = 'None' 
import matplotlib.pyplot as plt

import numpy as np
import time


# Initialize figure and screen thread
plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111)
print("    Launched graphical output")

axes = plt.gca()
axes.set_xlim([0,100])
axes.set_ylim([-200,220])

X_window = 300
x = np.linspace(0, X_window, X_window+1)
y = np.random.rand(np.size(x))*200
line1, = ax.plot(x, y, 'b-')
fig.canvas.draw()

time.sleep(5)


'''
from tkinter import *

window = Tk()

window.title("opENS")

lbl = Label(window, text="Hello")
lbl.grid(column=0, row=0)


lbl1 = Label(window, text="This is opENS")
lbl1.grid(column=1, row=2)


window.mainloop()
'''
