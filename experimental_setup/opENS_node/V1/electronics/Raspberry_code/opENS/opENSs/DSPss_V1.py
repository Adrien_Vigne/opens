#!/usr/bin/python3

# opENS DSP serial server (DSPss)
# 02-10-2020
# V1.0 -> from Toying Tank program with few functions to test
# V1.1 -> add mute function, fixed \00x or \0 end characters
# V1.2 -> add rawSendNFlush

import serial
import time
from datetime import datetime
import numpy as np

class DSPss:
  version = "1.0"
  status_connected = False
  debugOn = False #display more messages
  mute = False
  muteError = False
  
  defaultPort = '/dev/ttyUSB0' # default serial port
  ser = serial.Serial()  

  def msg(self, message): # display a formatted message in standard output
    if not self.mute:
      print("{} | {}".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"), message))

  def __init__(self, _port=defaultPort):
    self.msg("opENS DSP Serial Server, version {}".format(self.version))
    self.setPort(_port)
    return None
  
  def setPort(self, _port=defaultPort):
    self.port = _port
    self.msg("Serial port is set to '{}'".format(self.port))
    return True

  def connect(self):
    self.msg("Connection...")
    if self.status_connected:
      self.msg("Already connected!")
    try:
      self.ser.port = self.port
      self.ser.baudrate = 115200
      self.ser.bytesize = serial.EIGHTBITS #number of bits per bytes
      self.ser.parity = serial.PARITY_NONE #set parity check: no parity
      self.ser.stopbits = serial.STOPBITS_ONE #number of stop bits
      self.ser.timeout = .5            #non-block read
      self.ser.xonxoff = False     #disable software flow control
      self.ser.rtscts = False     #disable hardware (RTS/CTS) flow control
      self.ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
      self.ser.writeTimeout = 1     #timeout for write
      self.ser.open()
      time.sleep(0.5)
      self.ser.flushInput()
      self.ser.flushOutput()
      time.sleep(0.5)
      self.status_connected = True
      self.msg("Connected.")
    except Exception as e:
      self.msg("Error in 'connect' :")
      print(e)
      self.status_connected = False
    return self.version
      
  def disconnect(self):
    self.msg("Disconnection...")
    if not self.status_connected:
      self.msg("Already disconnected!")
    try:
      self.ser.close()
      self.status_connected = False
      self.msg("Disconnected.")
    except Exception as e:
      self.msg("Error in 'disconnect':")
      print(e)
      self.status_connected = False
    return self.status_connected

  def is_connected(self):
    self.msg("Status read: connected = {}".format(self.status_connected))
    return self.status_connected
  
  def _SerialSendNRead(self,request):
    response = ''
    try:
        self.ser.write(request.encode())
        response = self.ser.read_until(b"\x00",10).decode()
        response.replace('\0','')
        if self.debugOn:
          self.msg(">> {} >> {}".format(str(request),response))
        self.ser.flushInput()
    except Exception as e:
      self.msg("Error in '_SerialSendNRead':")
      if not self.muteError:
        print(e)
        print(request)
    return response


  #opENS routines  
  def get(self,Id):
    try:
        req = "G"+str(Id).zfill(3)+"\0"
        response = self._SerialSendNRead(req)
        return response
    except Exception as e:
      self.msg("Error in 'get':")
      print(e)
      return None

  def set(self,Id,Value):
    try:
        req = "S"+str(Id).zfill(3)+" "+str(Value)+"\0"
        response = self._SerialSendNRead(req)
        return response
    except Exception as e:
      self.msg("Error in 'get':")
      print(e)
      return None

  def get_version(self):
    try:
      req = "V\0"
      response = self._SerialSendNRead(req)
      self.msg("Version : {}".format(response))
      return response
    except Exception as e:
      self.msg("Error in 'get_version':")
      print(e)
      return None
  
  def rawSendNFlush(self,req_str):
    self.msg("Send request...")
    try:
      self.ser.write(req_str.encode())
    except Exception as e:
      print ("error communicating...: " + str(e))
    self.flush()
     

  def reset(self):
    self.msg("Send reset...")
    try:
      self.ser.write("R".encode())
      shit=self.ser.read_until('',300).decode()
      if self.debugOn:
        print(shit)
      self.msg("Reset done.")
      shit=self.ser.flushInput()
    except Exception as e:
      print ("error communicating...: " + str(e))

  def flush(self):
    self.msg("Flush connection buffers...")
    try:
      shit=self.ser.read_until('',300).decode()
      if self.debugOn:
        print(shit)
    except Exception as e:
      print ("error communicating...: " + str(e))







#######################
## To try and debug
'''

import DSPss_V1 as DSPss
me = DSPss.DSPss()
me.connect()

me.get(1)


me = DSPss()
me.connect()

me.servo_and_distance(100)

raw = me.stop_and_get_record()

print(raw)
print(type(raw))



me = TTss()
me.connect()

me.servo_and_distance(100)

me.home()
while me.is_homing():
  time.sleep(.5)

for i in range(25):
  t = time.time()
  angle = 40+i*2
  dist = me.servo_and_distance(angle) #lasts around 155ms!!!!!
  #print(angle)
  #print(dist)
  print(time.time()-t)
  while( time.time()-t < 0.05):
    time.sleep(0.01)

me.fast_servo(40)
time.sleep(0.5)

for i in range(25):
  t = time.time()
  angle = 40+i*2
  me.fast_servo(angle) #lasts around .35ms
  #print(angle)
  #print(dist)
  print(time.time()-t)
  while( time.time()-t < 0.02):
    time.sleep(0.01)


print(me.is_connected())

me.disconnect()

'''

