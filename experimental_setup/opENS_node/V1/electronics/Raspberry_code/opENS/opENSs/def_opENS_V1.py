#!/usr/bin/python3

# opENS definition
#  Contains constant and parameters
#
# V1: 2020-10-06

class def_opENS():
	sharedVars_V5 = {"MainState":[],"i_mem1":[],"i_mem2":[],"i_mem3":[],"configCalib":[],"iRefq":[],"iRefd":[],"id":[],"iq":[],"i0":[],"ia":[],"Iamoy":[],"Iaeff":[],"ib":[],"Ibmoy":[],"Ibeff":[],"ic":[],"Icmoy":[],"Iceff":[],"vaG":[],"Vamoy":[],"Vaeff":[],"vbG":[],"Vbmoy":[],"Vbeff":[],"vcG":[],"Vcmoy":[],"Vceff":[],"Vudcmoy":[],"vd":[],"vq":[],"v0":[],"kPdq":[],"kIdq":[],"ia_ref":[],"ib_ref":[],"ic_ref":[],"sinSyncSG":[],"DEBUGprobeVar1":[],"DEBUGprobeVar2":[],"DEBUGprobeVar3":[]}
	sharedVarsId_V5 = {"MainState":0,"i_mem1":1,"i_mem2":2,"i_mem3":3,"configCalib":4,"iRefq":5,"iRefd":6,"id":7,"iq":8,"i0":9,"ia":10,"Iamoy":11,"Iaeff":12,"ib":13,"Ibmoy":14,"Ibeff":15,"ic":16,"Icmoy":17,"Iceff":18,"vaG":19,"Vamoy":20,"Vaeff":21,"vbG":22,"Vbmoy":23,"Vbeff":24,"vcG":25,"Vcmoy":26,"Vceff":27,"Vudcmoy":28,"vd":29,"vq":30,"v0":31,"kPdq":32,"kIdq":33,"ia_ref":34,"ib_ref":35,"ic_ref":36,"sinSyncSG":37,"DEBUGprobeVar1":38,"DEBUGprobeVar2":39,"DEBUGprobeVar3":40}
	
	MainState = {'START_UP':0, 'IDLE_STATE':1, 'CALIB_SENSOR':2, 'STARTPFC':3, 'PARKCONTROL':4, 'OPENLOOP':5}
	
