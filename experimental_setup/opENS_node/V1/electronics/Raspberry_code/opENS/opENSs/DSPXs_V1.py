#!/usr/bin/python3

# opENS DSP XML RPC server (DSPXs)
# 01-10-2020
# V1.0 -> first try with function queue, no XML-RPC yet

import time
from datetime import datetime
import numpy as np
import threading
import queue
import DSPss_V1 as DSPss_
import def_opENS_V1 as def_opENS

class DSPXs:
	version = "1.0"
	
	# shared variables
	sharedVars = {}
	sharedVarsId = {}
	DSPversion = {'code':'?', 'shareVars':0} #initialized

	# set a queue of tasks to be executed one by one to avoid conflict in communicating with DSP
	DSPtaskQueue = queue.Queue()
	thread_DSPss = threading.Thread()
	
	# queue of tasks worker
	def DSPss_worker(self):
		while 1:
			task = self.DSPtaskQueue.get()
			try:
				exec(task)
			except Exception as e:
				print("DSPss task error :")
				print(task)
				print(e)
			self.DSPtaskQueue.task_done()

	#periodic data gathering
	data_monitoring_period = 0.75 #s >0.655 to work!
	data_monitor_running = False
	
	# periodic data monitoring function
	def data_monitor_function(self):
		if self.data_monitor_running: #shall we continue doing this?
			self.DSPtaskQueue.put("self.DSPgetShareVars()")
			threading.Timer(self.data_monitoring_period, self.data_monitor_function).start()

	# initialization function
	def __init__(self):
		print("opENS DSP interface class DSPXs")
		print("Version "+self.version)
		
		print('Load opENS defintions...')
		self.def_opENS = def_opENS.def_opENS()
		
		print("import opENS DSP serial server... ",end="")		
		self.DSPss = DSPss_.DSPss()
		print("Done")		
		self.DSPss.mute = True #to stop polluting the console.
		self.DSPss.muteError = True #to stop polluting the console.

		self.DSPconnect()
		
		print("Start daemon... ", end="")
		# turn-on the worker thread
		self.thread_DSPss = threading.Thread(target=self.DSPss_worker, 
										args=(), daemon=True)
		self.thread_DSPss.start()
		print("Done")
		
		#reset and flush DSP
		self.DSPtaskQueue.put("self.DSPreset()")
		
		#check version
		self.DSPtaskQueue.put("self.DSPgetVersion()")
		#wiat for these actions to be performed
		self.DSPtaskQueue.join()
		
		print("DSP version:")
		print(" code "+self.DSPversion['code'])
		print(" shareVars "+str(self.DSPversion['shareVars']))
		
		if self.DSPversion['code']=='?':
			raise ValueError('Could not get DSP code version. Connection problem? Try manual reset the DSP.')
		
		if self.DSPversion['shareVars']==5:
			self.sharedVars.clear()
			self.sharedVars = self.def_opENS.sharedVars_V5
			self.sharedVarsId = self.def_opENS.sharedVarsId_V5
		else:
			raise ValueError('shareVars not supported')
		
		# flush input buffer
		self.DSPtaskQueue.put("self.DSPss.flush()")
		#update dictionnary
		self.DSPtaskQueue.put("self.DSPgetShareVars()")
		#wait for update to be performed
		self.DSPtaskQueue.join()
		
		#start periodic data monitoring
		self.data_monitor_running = True
		threading.Timer(self.data_monitoring_period, self.data_monitor_function).start()
		
	########
	## DSP call functions

	def DSPconnect(self):
		# open connection
		self.DSPss.connect()
		self.DSPss.flush()

	def DSPdisconnect(self):
		# open connection
		self.DSPss.disconnect()
		self.data_monitor_running = False

	def DSPreset(self):
		#reset the DSP and flush serial
		self.DSPss.reset()
		self.DSPss.flush()
		
	def DSPgetVersion(self):
		# get and process version
		tmp = self.DSPss.get_version()
		tmp = tmp.replace('\r\x00','').split(' ') #remove end chars and split
		try:
			self.DSPversion['shareVars'] = int(tmp[1])
			self.DSPversion['code'] = tmp[0]
		except Exception as e:
			print('Error in formatting version')
			print(e)
	
	def DSPgetShareVars(self):
		t=time.time()
		# update all share vars
		for element in self.sharedVarsId:
			try:
				# remove all NULL chars
				tmp = self.DSPss.get(self.sharedVarsId[element]).replace('\0','')
				self.sharedVars[element] = eval(str(tmp))
			except Exception as e:
				self.sharedVars[element]=0
		#print("\033[94m DSPgetShareVars in "+str(time.time()-t)+"s\033[0m")
		# it takes 0.655s, almost constant +/-0.0006
		
		
		
	def dispShareVars(self):
		#print("DSPgetShareVars")
		# update all share vars
		for element in self.sharedVars:
			print(element+" = "+str(self.sharedVars[element]))
	
	#######
	## Application functions
	
	def getShareVars(self):
		return self.sharedVars
	
	def setShareVar(self,Var,Value):
		try:
			varId = self.sharedVarsId[Var]
		except Exception as e:
			print(str(e)+' is not a valid shared variable')
			return None
		data=str(Value)[0:6] # trunc to 6 digits including dot.
		self.DSPtaskQueue.put("self.DSPss.set({},{})".format(varId,data))

	def DSPrawSendNFlush(self,b_req):
		try:
			self.DSPtaskQueue.put("self.DSPss.rawSendNFlush('{}')".format(str(b_req)))
		except Exception as e:
			print(e)
			return None


	#######
	## Debug and test

	def test(self):
		print("Test")


#######
# try it

'''
me = DSPXs()
#print(me.version)

#me.test()

me.dispShareVars()
print('Ici on ne récupère que des trucs vides...')

time.sleep(1)

me.DSPdisconnect()
print("Finished")
'''
