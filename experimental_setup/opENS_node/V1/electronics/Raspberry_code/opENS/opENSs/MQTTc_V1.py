#!/usr/bin/python3

# opENS MQTT client
# 05-10-2020
# V1.0 -> setting everything up


import paho.mqtt.client as mqtt
import threading
import time
import json



class MQTTc():
	version = "1.0"
	
	defaultBroker = {"ip":"192.168.0.167", "port":1883}
	Broker = defaultBroker
	nodeId = 0 #Id of this opENS node
	nodeName = "A0"
	client = mqtt.Client()
	DSPXs = [] # linked to DSPXs object
	
	####################
	#periodic data publishing for supervision
	publishing_period = 1 #s
	publishToSupervisor = False #initial state = stop
	
	def data_publish_function(self):
		if self.publishToSupervisor: #shall we continue doing this?
			self.publishDict(self.DSPXs.getShareVars())
			threading.Timer(self.publishing_period, self.data_publish_function).start()
	
	
	####################
	# Initialization
	def __init__(self, nodeId=0, Broker=defaultBroker, DSPXs=[]):
		self.nodeId = nodeId;
		self.Broker = Broker;
		self.nodeName = "A"+str(self.nodeId)
		
		# raise error if DSPXs is not an instance of class DSPXs
		self.DSPXs = DSPXs
		self.DSPXs.version;
		
		# connect to MQTT broker
		print("MQTTc: connection to broker...",end="")
		
		self.client.connect(self.Broker["ip"],self.Broker["port"],60)
		tempDic = {"alive":1}
		self.client.publish(self.nodeName, json.dumps(tempDic))
		print("done")
		
		

	####################
	# Application functions
	
	def publishDict(self,Dict):
		#convert string values to actual values, maybe not required.
		tempDic=Dict
		for key,values in Dict.items(): #ayant enregistre dans communication.py dans des listes on a que des chaines de caracteres
			# print(key+","+str(type(eval(values))))
			try:
				tempDic[key]=eval(str(values))
			except Exception as e:
				tempDic[key]=[]
		self.client.publish(self.nodeName, json.dumps(tempDic))
	
	def disconnect(self):
		tempDic = {"alive":0}
		self.client.publish(self.nodeName, json.dumps(tempDic))
		self.client.disconnect();
	
	def startPublisherToSupervisor(self):
		self.publishToSupervisor = True
		threading.Timer(self.publishing_period, self.data_publish_function).start()
	
	def stopPublisherToSupervisor(self):
		self.publishToSupervisor = False
	
