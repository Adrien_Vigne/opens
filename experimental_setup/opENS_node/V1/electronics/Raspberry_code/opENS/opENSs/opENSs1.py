#!/usr/bin/python3

# opENS main server app
# 05-10-2020
# V1.0 -> setting everything up
# V1.1 -> supports calling from python3 console for manual command while app is running


print("\n              ______ _   _  _____\n             |  ____| \ | |/ ____|\n   ___  _ __ | |__  |  \| | (___\n  / _ \| '_ \|  __| | . ` |\___ \ \n | (_) | |_) | |____| |\  |____) |\n  \___/| .__/|______|_| \_|_____/\n       | |\n       |_|")
print ("Import packages...")
import time
import socket
import threading
import DSPXs_V1 as DSPXs
import MQTTc_V1 as MQTTc
import app2 as GUIapp

class opENSs():
	version = "1.1.1 Krouadur"
	
	# class variables
	localIP=''
	nodeId = 0
	DSPXs = []
	MQTTc = []
	GUIapp = []
	UserCode = []
	
	def getId(self):
		# get local IP to know which agent we are.
		self.localIP =  ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])
		print("Local IP : {}".format(self.localIP))
		
		self.nodeId = int(self.localIP.split('.')[-1])-100
		if self.nodeId<=0:
			raise ValueError('Invalid nodeId : {}'.format(str(self.nodeId)))
		print("Node Id : \033[92m{}\033[0m".format(self.nodeId))


	####################
	# Initialization
	def __init__(self, standalone=False):
		
		print("\033[92mopENS service version "+self.version+"\033[0m")
		
		
		self.getId()
		
		try:
			print("Start DSP service deamon...")
			self.DSPXs = DSPXs.DSPXs()
			print(" DSPXs version {}".format(self.DSPXs.version))
		except Exception as e:
			print('\033[91mFatal error\033[0m')
			print(e)
			return False

		try:
			print("Start MQTT client...")
			self.MQTTc = MQTTc.MQTTc(nodeId=self.nodeId,DSPXs=self.DSPXs)
			print(" MQTTc version {}".format(self.DSPXs.version))
			self.MQTTc.startPublisherToSupervisor()
		except Exception as e:
			print('\033[91mFatal error\033[0m')
			print(e)
			return False

		
		try:
			print("Start GUI application")
			self.GUIapp = GUIapp.GUIapp(self.DSPXs)
			self.GUIapp_thread = threading.Thread(target=self.GUIapp.start, daemon=True)
			self.GUIapp_thread.start()
		except Exception as e:
			print('\033[91mFatal error\033[0m')
			print(e)
			return False
		
		if standalone:
			print("Wait for GUI app to be close to end the application")
			self.GUIapp_thread.join()
			# here the GUI app is closed
			self.__del__()
		else:
			print("Do not forget to delete the object to correctly close the application")
		
		

	def __del__(self):
		print("Exiting application.")
		self.MQTTc.stopPublisherToSupervisor()
		time.sleep(1.5)
		self.MQTTc.disconnect()
		self.DSPXs.DSPdisconnect()

if __name__ == "__main__":
    opENSs(standalone=True)

# Example of code for manual PI tuning
#import opENSs1;  me=opENSs1.opENSs()
#me.DSPXs.DSPrawSendNFlush('p')
#me.DSPXs.sharedVars['kPdq']
#me.DSPXs.setShareVar('iRefd',0)
