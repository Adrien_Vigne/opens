#!/usr/bin/python3

# Pi app 0
# V0 = first test : GUI rigth size + plot
# V1 = towards operation : screen with buttons for Idle, Calib, DQ control, quit, Open Loop
# V1.1 modif on Pi
# V2 = moved to a functionnal class
version= 'V2'


print('##########\n\t opENS Pi GUI app {}\n######\n'.format(version))

#######################
# imported packages
#
from tkinter import * 
from tkinter import ttk
from tkinter import messagebox
import serial
import time
import threading
from matplotlib.figure import Figure 
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk) 
import def_opENS_V1 as def_opENS



class GUIapp():

  window=[] # GUI window
  DSPXs=[] # DSP interface object
  # labels that are updated are declared here
  I21 = [] #Id
  I22 = [] #Iq
  I23 = [] #Udc
  

  #############################
  ### Main function
  
  def __init__(self, DSPXs):
    # raise error if DSPXs is not an instance of class DSPXs
    self.DSPXs = DSPXs
    self.DSPXs.version;
    
    self.def_opENS = def_opENS.def_opENS()
  
  def start(self):
    
    print("\033[94m SGE app\033[0m")
        
    # invoke main window
    self.invokeMainWindow()
    
    # start updating display in 0.5s (wait a bit to start the GUI before).
    display_update_running = True
    threading.Timer(0.5, self.updateDisplay).start() 
    
    # run the gui 
    self.window.mainloop() 
    
    print("Exiting application")
    display_update_running = False

  ######
  ### Main window
  def invokeMainWindow(self):
    
    # the main Tkinter window 
    self.window = Tk()     
    # setting the title  
    self.window.title('opENS PiApp') 
    #remove title bar
    self.window.wm_attributes('-type', 'splash')
    # dimensions and color of the main window 
    self.window.geometry("320x240")
    cl = 'LightCyan2'
    self.window.configure(background=cl)
    
    #set grid
    for i in range(1,5):
      self.window.rowconfigure(i, weight=1)
      self.window.columnconfigure(i, weight=1)
    
    #GUI items
    #Id
    I11 = Button(master = self.window, text = '+', command = self.IdPlus, bg=cl, font=('Helvetica', 18, 'bold'))
    I11.grid(column=1, row=1, sticky=N+S+E+W)

    self.I21 = Label(master = self.window, text = 'Id ---A', bg=cl)
    self.I21.grid(column=1, row=2, sticky=N+S+E+W)

    I31 = Button(master = self.window, text = '-', command = self.IdMinus, bg=cl, font=('Helvetica', 18, 'bold'))
    I31.grid(column=1, row=3, sticky=N+S+E+W)
    #Iq
    I12 = Button(master = self.window, text = '+', command = self.IqPlus, bg=cl, font=('Helvetica', 18, 'bold'))
    I12.grid(column=2, row=1, sticky=N+S+E+W)

    self.I22 = Label(master = self.window, text = 'Iq ---A', bg=cl)
    self.I22.grid(column=2, row=2, sticky=N+S+E+W)

    I32 = Button(master = self.window, text = '-', command = self.IqMinus, bg=cl, font=('Helvetica', 18, 'bold'))
    I32.grid(column=2, row=3, sticky=N+S+E+W)
    #Udc
    self.I23 = Label(master = self.window, text = 'Udc ---V', bg=cl)
    self.I23.grid(column=3, row=2, columnspan = 2, sticky=N+S+E+W)
    #Idle button
    I33 = Button(master = self.window, text = 'Idle', command = self.Idle, bg=cl)
    I33.grid(column=3, row=3, columnspan = 2, sticky=N+S+E+W)
    #last line buttons
    I44 = Button(master = self.window, text = 'X', command = self.myExit, bg='IndianRed1', font=('Helvetica', 18, 'bold'))
    I44.grid(column=4, row=4, sticky=N+S+E+W)

    I41 = Button(master = self.window, text = 'Open Loop', command = self.SetOpenLoop, bg=cl)
    I41.grid(column=1, row=4, sticky=N+S+E+W)

    I42 = Button(master = self.window, text = 'Calibration', command = self.Calibration, bg=cl)
    I42.grid(column=2, row=4, sticky=N+S+E+W)

    I43 = Button(master = self.window, text = 'Start dq0', command = self.StartDQ0, bg=cl)
    I43.grid(column=3, row=4, sticky=N+S+E+W)

    I13 = Label(master = self.window, text = 'opENS', bg=cl, font=('Helvetica', 18, 'bold'))
    I13.grid(column=3, row=1, columnspan = 2, sticky=N+S+E+W)
    

  #################################################################################################
  #################################################################################################
  ## GUI routines

	#periodic data gathering
  display_update_period = 0.5 #s
  display_update_running = False
  
  # periodic update of displayed info
  def updateDisplay(self):
    if self.display_update_period: #shall we continue doing this?
      # retrieve variables
      data = self.DSPXs.getShareVars()
      # update labels
      self.I21.config(text="Id {:2.1f}A".format(data['id']))
      self.I22.config(text="Iq {:2.1f}A".format(data['iq']))
      self.I23.config(text="Udc {:3.1f}V".format(data['Vudcmoy']))
      # program next update
      threading.Timer(self.display_update_period, self.updateDisplay).start() 
  
  # ask before exiting
  def myExit(self):
    MsgBox = messagebox.askquestion ('Exit Pi App?','Exit opENS ?',icon = 'warning')
    if MsgBox == 'yes':
      self.window.destroy()

  # Id/Iq +/-
  def IqMinus(self):
    self.DSPXs.DSPrawSendNFlush('s')

  def IqPlus(self):
    self.DSPXs.DSPrawSendNFlush('z')

  def IdMinus(self):
    self.DSPXs.DSPrawSendNFlush('q')

  def IdPlus(self):
    self.DSPXs.DSPrawSendNFlush('a')


  # state control
  def Idle(self):
    self.DSPXs.setShareVar("MainState",self.def_opENS.MainState['IDLE_STATE'])
    
  def StartDQ0(self):
    self.DSPXs.setShareVar("MainState",self.def_opENS.MainState['STARTPFC'])

  def Calibration(self):
    self.DSPXs.setShareVar("MainState",self.def_opENS.MainState['CALIB_SENSOR'])

  # Open loop control
  def SetOpenLoop(self):
    MsgBox = messagebox.askquestion ('Open Loop voltage','Confirm order.',icon = 'warning')
    if MsgBox == 'yes':
      self.DSPXs.setShareVar("MainState",self.def_opENS.MainState['OPENLOOP'])




  '''
  # plot function is created for  
  # plotting the graph in  
  # tkinter window 
  def plot(window): 

    # the figure that will contain the plot 
    fig = Figure(figsize = (3, 2), 
                 dpi = 75) 

    # list of squares 
    y = [i**2 for i in range(101)] 

    # adding the subplot 
    plot1 = fig.add_subplot(111) 

    # plotting the graph 
    plot1.plot(y) 

    # creating the Tkinter canvas 
    # containing the Matplotlib figure 
    canvas = FigureCanvasTkAgg(fig, 
                               master = self.window)   
    canvas.draw() 

    # placing the canvas on the Tkinter window 
    canvas.get_tk_widget().pack() 

    # creating the Matplotlib toolbar 
    toolbar = NavigationToolbar2Tk(canvas, 
                                   window) 
    toolbar.update() 

    # placing the toolbar on the Tkinter window 
    canvas.get_tk_widget().pack() 
  '''


  #################################################################################################
