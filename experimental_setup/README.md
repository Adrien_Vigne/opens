**Experimental setup**

*WORK IN PROGRESS...*

The child folders contains the documents related to the opENS experimental setup.
They are organized as follow :
*  **opENS_node:** an open and low-cost 3-phase inverter,
*  **maman_supervision:** "mother" of the experiment, the sofware setup to supervise the experiment,
*  **power_components:** documentation of available power components,
*  **data:** some experimental results.


In more details:

**opENS_node** contains all the required information to reproduce the make opENS assets:
*  electronic hardware:
   *  schematics
   *  PCB making files
   *  Bill of materials
   *  EAGLE files
*  mechanical hardware:
   *  drawings
   *  making files
*  software
   *  all source code
   *  images for Raspberry Pi
*  test protocols
*  pictures and illustrations

**maman_supervision** contains the required information to configure the supervisor:
*  Linux configuration,
*  list of used package and configuration,
*  source code,
*  examples,
*  image.